#+TITLE: Coordination and Agreement
* System Model
* Consensus Problem
* [Consensus] Example
* Requirements of a Consensus Algorithm
* Solving Consensus in Absence of Failures
* On Conditions
* Solving Consensus in Presence of Crash Failures
* Consensus in a Synchronous System
* On Conditions
* Proof of Correctness
* Lower Bound
* Variant of Consensus: Byzantine Generals
* [Byzantine Generals] Requirements
* Variant of Consensus: Interactive Consistency
* Relating Consensus to Other Problems
* Suppose There Exists Solution to...
* Linking the Problems: IC from BG
* Linking the Problems: C from IC
* Linking the Problems: BG from C
* Byzantine Generals Problem in a Sync. System
* Impossibility with Three Processes: Scenario 1
* Impossibility with Three Processes: Scenario 2
* General Result: Impossibility with N ≤ 3f
* Solution with N ≥ 4 and f = 1
* Example: Scenario 1
* Example: Scenario 2
* Impossibility in Asynchronous Systems
