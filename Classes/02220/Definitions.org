#+TITLE: Definitions
#+LaTeX_HEADER: \theoremstyle{definition}
#+LATEX_HEADER: \newtheorem{definition}{Definition}[section]

* Example of Mobile Code Strategy
  :PROPERTIES:
  :ANKI_DECK: Distributed_systems
  :ANKI_NOTE_TYPE: Basic
  :ANKI_NOTE_ID: 1553343966819
  :END:

** Front
Show example and advantages of mobile code placement strategy
** Back
An advantage of running the downloaded code locally is that it can give good
interactive response since it does not suffer from the delays or variability of
bandwidth associated with network communication

* Proxy Server and Caches
  :PROPERTIES:
  :ANKI_DECK: Distributed_systems
  :ANKI_NOTE_TYPE: Basic
  :ANKI_NOTE_ID: 1553343966926
  :END:

** Front
Define is a proxy server and cache
** Back
A cache is a store of recently used data objects that is closer to one client or
a particular set of clients than the objects themselves
Purpose:
- To keep machines behind it anonymous (mainly for security)
- To speed up access to a resource (via caching)
Examples:
- Web browser
- Web proxy server

* Item
   :PROPERTIES:
   :ANKI_DECK: Distributed_systems
   :ANKI_NOTE_TYPE: Basic
   :ANKI_NOTE_ID: 1553248123964
   :END:
** Front
 What is an Indirect communication
** Back
 communication is indirect, through a third entity, allowing a strong degree
 of decoupling between senders and receivers, in particular:
 - space uncoupling: senders do not need to know who they are sending
 to
 - time uncoupling: senders and receivers do not need to exist at the
 same time
 Key techniques include: group communication, publish subscribe systems,
 message queues, tuple spaces, distributed shared memory (DSM)

** Item
   :PROPERTIES:
   :ANKI_NOTE_TYPE: Basic
   :ANKI_NOTE_ID: 1553343968468
   :END:
*** Front
List all type of placement strategies
*** Back
- Mapping of services to multiple servers
- Proxy server and caches
- Mobile code
** Item
   :PROPERTIES:
   :ANKI_NOTE_TYPE: Basic
   :ANKI_NOTE_ID: 1553343968542
   :END:
*** Front
What is *placement* and why it is important?
*** Back
How are entities mapped on to the physical distributed infrastructure (i.e.,
what is their placement)?

Placement is crucial in terms of determining the properties of the distributed
system, such as *performance*, *reliability* and *security*

** Item
   :PROPERTIES:
   :ANKI_NOTE_TYPE: Basic
   :ANKI_NOTE_ID: 1553262603767
   :END:
*** Front
Definition of Peer to peer Model Design
*** Back
All the processes involved in a task or activity play similar roles, interacting
cooperatively as *peers* without any distinction between client and server
processes or the computers that they run on.

** Item
   :PROPERTIES:
   :ANKI_NOTE_TYPE: Basic
   :ANKI_NOTE_ID: 1553262603923
   :END:
*** Front
Example of a server that is a client?
*** Back
- Web server client of a local file server that manages files
- Web servers and internet services are client of DNS
- Search engine, sever responds to queries from browser clients, client runs
  programs called web crawler that act as a client of other web servers

** Item
   :PROPERTIES:
   :ANKI_NOTE_TYPE: Basic
   :ANKI_NOTE_ID: 1553262604030
   :END:
*** Front
A process can be both a client and a server?
*** Back
Yes, but in general
- clients are active and server are passive
- server run continuously where clients lats only as long as the applications of
  which they from a part

** Item
    :PROPERTIES:
    :ANKI_NOTE_TYPE: Basic
    :ANKI_NOTE_ID: 1553248796138
    :END:
*** Front
What is an interprocess communication
*** Back
low level support for communication between processes in the distributed
system, including message-passing primitives, socket programming,
multicast communication
** Item
   :PROPERTIES:
   :ANKI_NOTE_TYPE: Basic
   :ANKI_NOTE_ID: 1553249644414
   :END:
*** Front
Define remote invocation
*** Back
most common communication paradigm, based on a two-way exchange
between communicating entities and resulting in the calling of a remote
operation (procedure or method
** Item
    :PROPERTIES:
    :ANKI_NOTE_TYPE: Basic
    :ANKI_NOTE_ID: 1553249644518
    :END:
*** Front
Define the two type of processes for a client server architecture style
*** Back
- *Server* process implementing a specific service (file system service database service)
- *Client* process that requests a service from a server by sending it a
            request ad subsequently waiting for the server’s reply
Possibly overlapping

