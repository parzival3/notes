* Embeded systems helping problems
- Why they are important
- Where we can find them
- How they can be improved
- Network and IoT
- New solutions
- Wild wild west
- The rise of new technologies

Nowadays embedded devices can be found everywhere, every person has a smartphone
or a smart-band that track his life activity and help them to communicate with
the world. Everyday we use at least one embedded system in order to accomplish
the most simple task. WIFI routers are embedded devices.

Moreover, what we want to accomplish is to make them communicate and interact
between them and this not as simple as it might seems.

In 2008 the number of devices that had access to the internet surpassed the
number of people present in the entaier planet. The fast technological
development, the simple usage of the devices and the reduction of cost for the
hardware gave to the consumer world technologies that not log time ago where
intended only for University and research. Nowadays most of the simple action that
we conduct everyday like using the public transportation, /another example/,
are controlled and enabled by embedded devices.

The next step that the world is heading to is to enable the communication
between those devices. IoT or Internet of Things is a paradigm, that refers to
the extension of the Internet to embedded devices. The idea behind it is that
devices whit sensor are able to communicate together and exchanging information
using the network. Like at the beginning of the Internet, nowadays there is not
a real standard for the IoT network.
Different protocol and different network topologies are begin proposed with
distinct goals and performance.

Given the large variety of scenarios and goals that the IoT is targeting,
finding a standard that is capable of adapting in a dynamic way to all of them
is not easy. The traditional wireless technologies where not designed to satisfy
the dynamism of this paradigm. A new concept of network was born called LPWAN or
Low power wide are network. This type of networks are designed to enable the
communication between devices that are placed in a vast area, while keeping the
energy required to transmit the information very low. In this report we focused
on the LoRa LPWAN technology and its open protocol LoRaWAN.

* Distributed systems and IOT, embeded systems, LPWAN
* LPWAN
To bridge the gap between the exsisting technologies and the necessity to
connect millions of devices together, the concept of Low Power Wide Area Network
was born. The LPWAN networks represent a innovative model of communication which
integrates the traditional cellular networks technologies with the short range
wireless technologies in order to achieve the different needs of the IoT world.

On the contrary of the traditional wireless technologies, LPWAN networks
sacrifiecs the maximum throughtput of the devices in order to manage a large
number of nodes simultaniuously. Since LPWAN is a concept of how a network
works, different implementation and different competitor are trying to acchieve
the same result using different technologies. The major competitor in this field
are NB-IoT, EC-GSM-IoT, LTE-M, SigFox and Lora. The first three as the name
suggest are use the traditional cellular network infrasctruture, while LoRa and
SigFox use the ISM (Industrial, Scientific and Medical) frequency.

* Lora and LoraWAN
LoRa and LoRaWAN is a semi-propretary technology developed by Semtech. This
technology is composed by a propetary physical Layer with the name of LoRa and a
open source protocol called LoRaWAN which utilize the physical layer LoRa. LoRa
implements a type of communication based on Spread Specturm which enable the
communication between devices that are kilomiters away from the gateway.
The key point of this technology are the large comunication range, the low power
consumption and the ability to adapt the in a dynamic way the data rate based on
the position of the node relative to the gateway. Given the open comunication
protocol LoRaWAN, it is possible to develop public or private networks without
having to buy any type of license, moreover, using the ISM band means that the
only cost for starting a network is buy the infrastructure. Project like The
Things Networks are trying to crate a public or private LoRa network.

** LoRaWAN
Based on the physical layer LoRa, LoRaWAN is a MAC protocol or /media access
control/ open source develop for the LPWAN networks. Standardaized by the LoRa
alliance, it has the goal to create a bridge between the *device layer* with the
*application layer*. Comparing the LoRaWAN protocol with the OSI model, one can
see that it can be collocated in the second and third layer of the model.

In order to enable a grate number of devices connected at the same time, LoRaWAN
is based on a start topology network. In this gerarchich topology, the end
devices communicate with the application layer only through the gateways, which
translate the LoRa package received by the node to UDP/TCP package that can be
send to a server. Given the topology, each message sent by an end device can be
received by one or more gateways, so it is the server duty to detect the
duplicate messages and select the closest gateway to send back a response.

In the LoRaWAN specifications are defined three types of classes of devices
born for the different type of use cases. The main class is the A class, each
device implement this class and this is the class that is focused on energy efficiency.
The other tow classes B, C are an extension of the A class, this two classes are
reserved for devices that are directly connected on the power grid or that uses
external power sources.

- Class A :: is the default operating mode. In this mode the node communicates
             in a ashynchronous way with the gateway. In this mode are
             implemented two different window in which the node listen to the
             gateway. If the gateway doesn't answer in this two windows, it
             needs to wait untill the device send a new message in order to
             communicate with it.
- Class B :: These are devices that extends the functionalities of the class A.
             This devices are synchronized with the base station with bacon
             messages send to the gateway. Thanks to these messages the gateway
             is able to communicate with the device at fixed intervals.
- Class C :: Is an extension that enable the device to always listen to the
             gateway messages.

** Frequency Bands
The LoRa technology operates in the free band of the radio spectrum. Like the
most used wireless technologies, WIFI, Bluetooth, ZigBee, also LoRa can be used
by the user without the necessity of a license. /A drawback of this bands is the
fact that the devices that operates in it have a limited power transmission and
cannot occupy the transmission channel for a long period of time/. LoRa and the
LoRaWAN protocol support all the three different ISM region bands used in
Europe, China and US.
/Insert table of ISM bands/

** Pacchetto LoRaWAN
The LoRaWAN packet is composed like in /figure --/ Inside the *PHYPayload* is
the LoRaWAN message which is structured like:
- MHDR :: The *MAC* header is the first part of the packet and it is divided in
          three sub-part. The first one specify the type of the message in the
          *MType* message, which can be of six different type represented by 3
          bit each. The second one is *Major* which specify the format of the
          messages exchanged by the join procedure and the last one is *RFU*
          which means reserved for future usage.
- MACPayload :: Is composed by the *Frame header* (FHDR) which specify to which
                node the packet is sent/arrives and it is used for the ADR
                algorithm. The second field is the *Frame counter* in which are
                present two counters, one for the number of messages in uplink
                (FCntUP) sent by the node, and a second one for the packet
                received (FCntDown). In this frame is also present a field
                called *FPort* which is optional and indicates the presence of
                the FRMPayload where the information is stored.
- MIC :: *Message Integrity Check* is a code that is calculated for each packet
         and it used to very the integrity of the message.


In order to menage the network there are some MAC commands that are exclusively
exchanged by the server and the MAC layer of an end-device. This messages are
not visible by the application in the server or by the nodes. The most important
messages are *LinkCheckReq* which is used by a node to validate the connection
with the network and *LinkADRReq* which is a request for the node to change the
data-rate, the power transmission, channel o rate of transmission.

** Adaptive data rate
Adaptive Data Rate o ADR is a mechanism used for optimizing the data rate of the
various end devices in a dynamic way. This mechanism is composed by an algorithm
implemented in the application layer is capable of twiking various parameter of
the node in order to achieve the maximum performance and increasing the battery
life of the node. The ADR request is issued by the end devices to the
application layer, since the first request the application layer will start
collecting the last 20 transmission send by the node. Based on the data
acquired, the ADR algorithm is capable of optimizing the connection with the
node in order to change the bandwidth and the transmission power. The parameter
used by the ADR are the *Frame counter* the number of gateways and the signal to
noise ratio with which the message arrived. Since the ADR use these three
parameters, this algorithm can be useful for only the node that are set in a
fixed position or to a node that has a period of mobility very limited.
The calculation of the various parameters for the various devices is not an easy
task, in fact, this parameters change based on the region in which the devices
are located and healt of the network. Since the protocol is open, different
application can use a different algorithm to calibrate the parameters of the
devices.

** Security
An important aspect that is not underestimated by the LoRaWAN protocol is the security.
Each node implement two different security keys *AppSkey* and *NwkSkey* which
are encrypted with the AES 128 bits protocol. The network session key (NwKSKey)
is the key that is utilized in order to provide the security on the
communications between the devices in the network. This key is used to verify
the validity of the messages with the MIC (Message Integrity Check) procedure.
L'application section key (AppSkey) is used for the encryption and decryption of
the payload of the messages. Whit this key is assured the security in the
message passing between the application layer and the end device.
This key are unique and are generated each time the devices is turned off or it
change the network.
** Limitations and scalability
Being a very young technology, it is difficult to estimate the potentials and
the limits concerned LoRa and LoRaWAN. A crucial point of this technology is the
scalability factor and the number of simultaneous communications that a single
gateway can handle. In this regards it is difficult to estimate precisely the
number involved, given the fact that many factors can influence the performance of
the network. Some theoretical research based on simulations was proposed in the
paper \cite{inproceedings}, in which the throughput available of a single
LoRa end device and the number of node supported by a single gateway in
different scenarios are tested. The conclusion from this paper are that LoRa can
be used for covering scenarios where a high coverage is needed and that this
technology is satisfactory scalabel if the uplink traffic is low. It is also
show that this technology has a poor reliability and substantial delay which
means that it it cannot be used for safety critical applications.
