clock time;

broadcast chan moveWaitingTrainsToNewSection, trainArrived;

const int number_of_trains = 2;
const int number_of_points = 1;
const int number_of_sections = 3;
const int number_of_signals = 2;

typedef int[0,3] section_Id;
typedef int[0,2-1] signals_Id;
typedef int[0,2] signals_Idp1;
typedef int[0] point_Id;
typedef int[0,1-1] point_Idp1;
typedef int[0,2-1] train_Id;
typedef int[0,1] onoff;
typedef int[0,50] rate_id;

const rate_id section_rate[number_of_sections] = {30,15, 30};

rate_id train_rate[number_of_trains] = {26,1};

const int goal[number_of_trains] = {1,0};

bool direction [number_of_trains];
bool config_Of_Points [number_of_points];
onoff config_Of_Lights [number_of_lights];
bool occupied[number_of_sections];
bool exited[number_of_trains];
bool crash = false;

const lights_Idp1 relevantLightsForRightTrains[section_Id] = {0, -1, -1, 0};
const lights_Idp1 relevantLightsForLeftTrains[section_Id] = {-1, 1, -1, 0};
const point_Idp1 relevantPointsForLeftTrains[section_Id] = {0, 0, -1, 0};
const point_Idp1 relevantPointsForRightTrains[section_Id] = {-1, -1, 0, 0};


void initialisePositions() {
        position[0] = 0;
        occupied[0] = 1;
        direction[0] = 1;
        position[1] = 2;
        occupied[1] = 1;
        direction[1] = 0;
}

// At the beginning we need to initialize all the light and the points
void initialiseAllPointsAndLights() {
        int p; int l;

        while (p < number_Of_Points) {
                config_Of_Points [p] = 0;
                p++;
        }
        while (l < number_Of_Lights) {
                config_Of_Lights [l] = 1;
                l++;
        }
}

// Each time is the Eva turn we need to update our state
void initialiseAllPointsAndLightsNew() {
        int p; int l;
        section_Id pos0 = position[0];
        section_Id pos1 = position[1];
        // For all the points if the current point P is not relevant for the trains
        // We need to modify this if both train go to the same direction
        while (p < number_Of_Points) {
                if (p != relevantPointsForRightTrains[pos0] and
                    p != relevantPointsForLeftTrains[pos1])  config_Of_Points [p] = 0;
                p++;
        }
        // This update the light next to the trains
        while (l < number_Of_Lights) {
                if (l != relevantLightsForRightTrains[pos0] and l != relevantLightsForLeftTrains[pos1]) {
                        config_Of_Lights [l] = 1; // 1 is Red and 0 is Green
                }
                l++;
        }
}

// Check if the train reach the goal, if it reached it we can remove the train
// from the position and set the position not occupied
void checkForGoal(train_Id tId) {
        if (position[tId] == goal[tId]) {
                exited[tId] = true;
                occupied[position[tId]] = false;
        }
}

// Check wich move can do the player
bool canMove(train_Id tId) {
        section_Id pos = position[tId];
        bool dir = direction[tId];
        if (exited[tId]) return false;
        if (pos == 0 and dir == 0) return true;
        if (pos == 0 and dir == 1 and config_Of_Lights[0] == 0) return true;
        if (pos == 1 and dir == 0 and config_Of_Lights[1] == 0) return true;
        if (pos == 1 and dir == 1) return true;
        if (pos == 2 and dir == 0) return true;
        if (pos == 2 and dir == 1) return true;
        return false;
}

// Check the direction of the train in the given section
bool direction_Of_Train_In_Section(int s) {
        int t = 0;
        while (t < number_Of_Trains) {
                if (position[t] == s)
                        return direction[t];
                t++;
        }
        return 0;
}

void setRelevantLights(onoff q0,onoff q1) {
        section_Id pos0 = position[0];
        section_Id pos1 = position[1];
        initialiseAllPointsAndLightsNew();
        if (relevantLightsForRightTrains[pos0] != -1)
                config_Of_Lights[relevantLightsForRightTrains[pos0]] =
                        config_Of_Lights[relevantLightsForRightTrains[pos0]] && q0;
        if (relevantLightsForLeftTrains[pos1] != -1)
                config_Of_Lights[relevantLightsForLeftTrains[pos1]] =
                        config_Of_Lights[relevantLightsForLeftTrains[pos1]] && q1;
}


void setRelevantPointPositions(onoff p0,onoff p1) {
        section_Id pos0 = position[0];
        section_Id pos1 = position[1];
        if (relevantPointsForRightTrains[pos0] != -1
           && (relevantLightsForRightTrains[pos0] == -1
           || config_Of_Lights[relevantLightsForRightTrains[pos0]] == 0)) {
                if (pos0 == 2)
                        config_Of_Points[relevantPointsForRightTrains[pos0]] = 0;
                else if (pos0 == 4)
                        config_Of_Points[relevantPointsForRightTrains[pos0]] = 0;
                else if (pos0 == 5)
                        config_Of_Points[relevantPointsForRightTrains[pos0]] = 0;
                else
                        config_Of_Points[relevantPointsForRightTrains[pos0]] = p0 ||
                                config_Of_Points[relevantPointsForRightTrains[pos0]];
        }

        if (relevantPointsForLeftTrains[pos1] != -1
            && (relevantLightsForLeftTrains[pos1] == -1 ||
                config_Of_Lights[relevantLightsForLeftTrains[pos1]] == 0)) {
                if (pos1 == 1)
                        config_Of_Points[relevantPointsForLeftTrains[pos1]] = 0;
                else config_Of_Points[relevantPointsForLeftTrains[pos1]] = p1 ||
                             config_Of_Points[relevantPointsForLeftTrains[pos1]];

        }
}
