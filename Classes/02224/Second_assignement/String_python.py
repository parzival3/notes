import re

reg_state = r"state\n(.*)\n"
reg_light = r"(config_Of_Lights\[\d+\]=\d)"
reg_point = r"(config_Of_Points\[\d+\]=\d)"
reg_occu = r"(occupied\[\d+\]=\d)"
reg_exited = r'exited\[\d+\]=\d'

with open('/tmp/output.txt') as fp:
    content = fp.read()
    fp.close()
states = re.findall(reg_state, content)

for state in states:
    lights = re.findall(reg_light, state)
    points = re.findall(reg_point, state)
    occupi = re.findall(reg_occu, state)
    exiteds = re.findall(reg_exited, state)
    for light in lights:
       print(light)
    for point in points:
       print(point)
    for occup in occupi:
       print(occup)
    for exited in exiteds:
       print(exited)
    print("---------------------------------------------------")
