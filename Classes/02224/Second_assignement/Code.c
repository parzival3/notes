clock time;

broadcast chan moveWaitingTrainsToNewSection, trainArrived;
/**
 * We start by initializing all the possible position lights and points to zero
 * Then we set all the lights to RED -> 1
 * 
 */
const int number_Of_Trains = 2; // The rest depends on this variable been two
const int number_Of_Points = 6;
const int number_Of_Sections = 11;
const int number_Of_Lights = 14;

typedef int[0,11] section_Id;
typedef int[0,14-1] lights_Id;
typedef int[0,14] lights_Idp1;
typedef int[0,6-1] point_Id;
typedef int[0,6] point_Idp1;
typedef int[0,2-1] train_Id;
typedef int[0,1] onoff;
typedef int[0,50] rate_id;

const rate_id section_rate[number_Of_Sections] = {26,1,15,15,1,15,26,15,26,1,26} ;

rate_id train_rate[number_Of_Trains] = {26,1};
section_Id position[number_Of_Trains];

const int goal[number_Of_Trains] = {6,0};

bool direction [number_Of_Trains];
bool config_Of_Points [number_Of_Points];
onoff config_Of_Lights [number_Of_Lights];
bool occupied[number_Of_Sections];
bool exited[number_Of_Trains];
bool crash=false;

// The number 14 means that the section doesn't have a light
const lights_Idp1 relevantLightsForRightTrains[section_Id] = {1, 5, 14, 14, 3, 14, 7, 14, 9, 11, 13,0};
const lights_Idp1 relevantLightsForLeftTrains[section_Id] = {0, 2, 14, 14, 4, 14, 6, 14, 8, 10, 12,0};
const point_Idp1 relevantPointsForLeftTrains[section_Id] = {6, 0, 0, 4, 1, 3, 2, 3, 6, 4, 5,0};
const point_Idp1 relevantPointsForRightTrains[section_Id] = {0, 2, 1, 1, 3, 2, 6, 5, 4, 5, 6,0};

void initialisePositions() {
        position[0] = 0;
        occupied[0] = 1;
        direction[0] = 1;
        position[1] = 1;
        occupied[1] = 1;
        direction[1] = 0;
}

// At the beginning we need to initialize all the light and the points
void initialiseAllPointsAndLights() {
        int p; int l;

        while (p < number_Of_Points) {
                config_Of_Points [p] = 0;
                p++;
        }
        while (l < number_Of_Lights) {
                config_Of_Lights [l] = 1;
                l++;
        }
}

// Each time is the Eva turn we need to update our state
void initialiseAllPointsAndLightsNew() {
        int p; int l;
        section_Id pos0=position[0];
        section_Id pos1=position[1];
        // For all the points if the current point P is not relevant for the trains
        // We need to modify this if both train go to the same direction
        while (p < number_Of_Points) {
                if (p != relevantPointsForRightTrains[pos0] and
                    p != relevantPointsForLeftTrains[pos1])  config_Of_Points [p] = 0;
                p++;
        }
        // This update the light next to the trains
        while (l < number_Of_Lights) {
                if (l != relevantLightsForRightTrains[pos0] and l != relevantLightsForLeftTrains[pos1]) {
                        config_Of_Lights [l] = 1; // 1 is Red and 0 is Green
                }
           l++;
        }
}

// Check if the train reach the goal, if it reached it we can remove the train
// from the position and set the position not occupied
void checkForGoal(train_Id tId) {
        if (position[tId] == goal[tId]) {
                exited[tId] = true;
                occupied[position[tId]] = false;
        }
}

// Check wich move can do the player
bool canMove(train_Id tId) {
        section_Id pos = position[tId];
        bool dir = direction[tId];
        if (exited[tId]) return false;
        if (pos == 0 and dir == 0 and config_Of_Lights[0] == 0) return true;
        if (pos == 0 and dir == 1 and config_Of_Lights[1] == 0) return true;
        if (pos == 1 and dir == 0 and config_Of_Lights[2] == 0) return true;
        if (pos == 1 and dir == 1 and config_Of_Lights[5] == 0) return true;
        if (pos == 2 and dir == 0) return true;
        if (pos == 2 and dir == 1) return true;
        if (pos == 3 and dir == 0) return true;
        if (pos == 3 and dir == 1) return true;
        if (pos == 4 and dir == 0 and config_Of_Lights[4] == 0) return true;
        if (pos == 4 and dir == 1 and config_Of_Lights[3] == 0) return true;
        if (pos == 5 and dir == 0) return true;
        if (pos == 5 and dir == 1) return true;
        if (pos == 6 and dir == 0 and config_Of_Lights[6] == 0) return true;
        if (pos == 6 and dir == 1 and config_Of_Lights[7] == 0) return true;
        if (pos == 7 and dir == 0) return true;
        if (pos == 7 and dir == 1) return true;
        if (pos == 8 and dir == 0 and config_Of_Lights[8] == 0) return true;
        if (pos == 8 and dir == 1 and config_Of_Lights[9] == 0) return true;
        if (pos == 9 and dir == 0 and config_Of_Lights[10] == 0) return true;
        if (pos == 9 and dir == 1 and config_Of_Lights[11] == 0) return true;
        if (pos == 10 and dir == 0 and config_Of_Lights[12] == 0) return true;
        if (pos == 10 and dir == 1 and config_Of_Lights[13] == 0) return true;
        return false;
}

// Check the direction of the train in the given section
bool direction_Of_Train_In_Section(int s) {
        int t = 0;
        while (t<number_Of_Trains) {
                if (position[t] == s)
                        return direction[t];
                t++;
        }
        return 0;
}

void setRelevantLights(onoff q0,onoff q1) {
        section_Id pos0 = position[0];
        section_Id pos1 = position[1];
        initialiseAllPointsAndLightsNew();
        if (relevantLightsForRightTrains[pos0] != 14)
                config_Of_Lights[relevantLightsForRightTrains[pos0]] =
                        config_Of_Lights[relevantLightsForRightTrains[pos0]] && q0;
        if (relevantLightsForLeftTrains[pos1] != 14)
                config_Of_Lights[relevantLightsForLeftTrains[pos1]] =
                        config_Of_Lights[relevantLightsForLeftTrains[pos1]] && q1;
}


void setRelevantPointPositions(onoff p0,onoff p1) {
        section_Id pos0 = position[0];
        section_Id pos1 = position[1];
        /**
         * This checks the points for train 0
         * If the train is in a position with one point and the position has no light or the light is GREEN
         * 
         */
        if (relevantPointsForRightTrains[pos0] != 6
           && (relevantLightsForRightTrains[pos0] == 14
           || config_Of_Lights[relevantLightsForRightTrains[pos0]] == 0)) {
                if (pos0 == 2)
                        config_Of_Points[relevantPointsForRightTrains[pos0]] = 0;
                else if (pos0 == 4)
                        config_Of_Points[relevantPointsForRightTrains[pos0]] = 0;
                else if (pos0 == 5)
                        config_Of_Points[relevantPointsForRightTrains[pos0]] = 1;
                else
                        config_Of_Points[relevantPointsForRightTrains[pos0]] = p0 ||
                                config_Of_Points[relevantPointsForRightTrains[pos0]];
        }

        /**
         * this checks the points for train 1
         */
        if (relevantPointsForLeftTrains[pos1] != 6
            && (relevantLightsForLeftTrains[pos1] == 14 ||
                config_Of_Lights[relevantLightsForLeftTrains[pos1]] == 0)) {
                if (pos1 == 1)
                        config_Of_Points[relevantPointsForLeftTrains[pos1]] = 0;
                else config_Of_Points[relevantPointsForLeftTrains[pos1]] = p1 ||
                             config_Of_Points[relevantPointsForLeftTrains[pos1]];

        }
}

section_Id go(int tId) {
        // We can modify this and make it like
        // bool right = poistion[tid]? true : false;
        int pos = position[tId];
        bool dir = direction[tId];
        if (po s == 0 and dir == 0) {
                crash=true;
                return pos;
        }
        if (pos == 0 and dir == 1) {
                if (config_Of_Points[0] == 0) {
                        if (occupied[1] == 0) {
                                occupied[1] = 1;
                                occupied[0] = 0;
                                return 1;
                        } else {
                                crash = true;
                                return pos;
                        }
                } else {
                        if (occupied[2] == 0) {
                                occupied[2] = 1;
                                occupied[0] = 0;
                                return 2;
                        } else {
                                crash = true;
                                return pos;
                        }
                }
        }
        if (pos == 1 and dir == 0) {
                if (occupied[0] == 0) {
                        occupied[0] = 1;
                        occupied[1] = 0;
                        return 0;
                } else {
                        crash = true;
                        return pos;
                }
        }
        if (pos == 1 and dir == 1) {
                if (occupied[6] == 0) {
                        occupied[6] = 1;
                        occupied[1] = 0;
                        return 6;
                } else {
                        crash = true;
                        return pos;
                }
        }
        if (pos == 2 and dir == 0) {
                if (occupied[0] == 0) {
                        occupied[0] = 1;
                        occupied[2] = 0;
                        return 0;
                } else {
                        crash = true;
                        return pos;
                }

        }
        if (pos == 2 and dir == 1) {
                if (occupied[4] == 0) {
                        occupied[4] = 1;
                        occupied[2] = 0;
                        return 4;
                } else {
                        crash = true;
                        return pos;
                }
        }
        if (pos == 3 and dir == 0) {
                if (occupied[8] == 0) {
                        occupied[8] = 1;
                        occupied[3] = 0;
                        return 8;
                } else {
                        crash = true;
                        return pos;
                }
        }
        if (pos == 3 and dir == 1) {
                if (occupied[4] == 0) {
                        occupied[4] = 1;
                        occupied[3] = 0;
                        return 4;
                } else {
                        crash = true;
                        return pos;
                }
        }
        if (pos == 4 and dir == 0) {
                if (config_Of_Points[1] == 0) {
                        if (occupied[3] == 0) {
                                occupied[3] = 1;
                                occupied[4] = 0;
                                return 3;
                        } else {
                                crash = true;
                                return pos;
                        }
                } else {
                        if (occupied[2] == 0) {
                                occupied[2] = 1;
                                occupied[4] = 0;
                                return 2;
                        } else {
                                crash = true;
                                return pos;
                        }
                }
        }
        if (pos == 4 and dir == 1) {
                if (config_Of_Points[3] == 0) {
                        if (occupied[5] == 0) {
                                occupied[5] = 1;
                                occupied[4] = 0;
                                return 5;
                        } else {
                                crash = true;
                                return pos;
                        }
                } else {
                        if (occupied[7] == 0) {
                                occupied[7] = 1;
                                occupied[4] = 0;
                                return 7;
                        } else {
                                crash = true;
                                return pos;}
                }
        }
        if (pos == 5 and dir == 0) {
                if (occupied[4] == 0) {
                        occupied[4] = 1;
                        occupied[5] = 0;
                        return 4;
                } else {
                        crash = true;
                        return pos;
                }
        }
        if (pos == 5 and dir == 1) {
                if (occupied[6] == 0) {
                        occupied[6] = 1;
                        occupied[5] = 0;
                        return 6;
                } else {
                        crash = true;
                        return pos;
                }
        }
        if (pos == 6 and dir == 0) {
                if (config_Of_Points[2] == 0) {
                        if (occupied[1] == 0) {
                                occupied[1] = 1;
                                occupied[6] = 0;
                                return 1;
                        } else {
                                crash = true;
                                return pos;
                        }
                } else {
                        if (occupied[5] == 0) {
                                occupied[5] = 1;
                                occupied[6] = 0;
                                return 5;
                        } else {
                                crash = true;
                                return pos;
                        }
                }
        }
        if (pos == 6 and dir == 1) {
                crash=true;
                return pos;
        }
        if (pos == 7 and dir == 0) {
                if (occupied[4] == 0) {
                        occupied[4] = 1;
                        occupied[7] = 0;
                        return 4;
                } else {
                        crash = true;
                        return pos;
                }
        }
        if (pos == 7 and dir == 1) {
                if (occupied[10] == 0) {
                        occupied[10] = 1;
                        occupied[7] = 0;
                        return 10;
                } else {
                        crash = true;
                        return pos;
                }
        }
        if (pos == 8 and dir == 0) {
                crash=true;
                return pos;
        }
        if (pos == 8 and dir == 1) {
                if (config_Of_Points[4] == 0) {
                        if (occupied[9] == 0) {
                                occupied[9] = 1;
                                occupied[8] = 0;
                                return 9;
                        } else {
                                crash = true;
                        return pos;
                        }
                } else {
                        if (occupied[3] == 0) {
                                occupied[3] = 1;
                                occupied[8] = 0;
                                return 3;
                        } else {
                                crash = true;
                                return pos;
                        }
                }
        }
        if (pos == 9 and dir == 0) {
                if (occupied[8] == 0) {
                        occupied[8] = 1;
                        occupied[9] = 0;
                        return 8;
                } else {
                        crash = true;
                        return pos;
                }
        }
        if (pos == 9 and dir == 1) {
                if (occupied[10] == 0) {
                        occupied[10] = 1;
                        occupied[9] = 0;
                        return 10;
                } else {
                        crash = true;
                        return pos;
                }
        }
        if (pos == 10 and dir == 0) {
                if (config_Of_Points[5] == 0) {
                        if (occupied[9] == 0) {
                                occupied[9] = 1;
                                occupied[10] = 0;
                                return 9;
                        } else {
                                crash = true;
                                return pos;
                        }
                } else {
                        if (occupied[7] == 0) {
                                occupied[7] = 1;
                                occupied[10] = 0;
                                return 7;
                        } else {
                                crash = true;
                                return pos;
                        }
                }
        }
        if (pos == 10 and dir == 1) {
                crash=true;
                return pos;
        }
        return pos;
}

