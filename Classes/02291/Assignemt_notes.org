#+TITLE: MUD GAME
* Exercise number one
** Creating a domain model
*** Create a glossary
Thing that we need to explain the game to other non technical people.
 - Rooms
   - Special
   - Start room
   - Regular room
   - Can have many objects
 - Levels
   - Initial level
   - Final level
 - Player
   - Take object
   - Trade object
   - Discard player
   - Multi player
   - Chatting
   - Trading
 - Inventory
   - Inventory capacity of 5 items
 - Objects
   - Inventory objects
   - Rooms Objects
   - Take objects
   - Lay down objects
 - Infrastructure
   - Server
   - Join the game
*** Create a class diagram for the domain model

** Describe the functional requirements using use case
*** Create one or more use-case diagram
*** Provide 2 detailed use-case
*** Describe the non-functional requirements in text form
**** Are they testable
*** Create acceptance test using Fit tables for the detailed uses cases
