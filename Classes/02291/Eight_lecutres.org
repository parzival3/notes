#+TITLE: EIGHT LECTURE
* Design By Contract and OCL
Easy to use specification language developed by UML
** What does this function do?
Quick short, because it select a pivot element and split int less equal and
bigger and then it iterates itself.
** Contract for the sort function in OCL
** Contracts
** Example
** Example
** Object Constraint Language (OCL)
** Bank example with constraints
** Update operation of Account
** Update operation of Account
** OCL Syntax
** OCL expressions and their evaluation
** Type of constraints
** Notation
** OCL Syntax (simplified): Available types
** OCL Syntax (simplified): Basic operations
** Navigation
** Navigation
** OCL Syntax (simplified): OCL Boolean Operators and
** OCL Syntax (simplified): Operations on collections I
** OCL Syntax (simplified): Operations on collections II
** Flattening Collections
** OCL Syntax (simplified): Postconditions
** Observer Pattern
** Observer Pattern
** Observer Pattern
** Observer Pattern
** Observer Pattern
** Observer Pattern
** Observer Pattern
** Observer Pattern
** Observer Pattern Final
** Example Vending Machine
** Vending machine behaviour with OCL constraints
** Vending machine behaviour as life cycle state machine
* Principles of Good Design
** Contents
** DRY principle
** Example: Code Duplication
** Example: Code Duplication
** DRY principle
** KISS principle
** YAGNI principle
** Low Coupling
** Low Coupling
** High Cohesion
** High Cohesion
** Law of Demeter
** Computing the price of an order
** Computing the price of an order
** Layered Architecture
** Example Vending Machine
** Architecture
** Presentation Layer: Swing GUI
** Presentation Layer: Command Line Interface
** Advantages of the separation
