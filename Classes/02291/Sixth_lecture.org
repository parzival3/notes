#+TITLE: UML State Machines

Are diagram to show the single behavior of a object of the system.
The diagram is composed by
- state :: square boxes
- transitions :: which are rule with which the object change state, each
                 transition has a label in front of them that has three part
                 which all of them are optional trigger-state [guard]/activity.
  - trigger-state is usually a single event that triggers the
    change of a state. If no trigger-state is present it meas that the
    transition is take immediately.
  - the guard is a boolean condition that must be true in order to take the transition
  - the activity represent some behavior that occurs during the transition,
    update a value or setting some flags.
If an event occurs and no transition is valid the event is ignored. When we
reach the final state we can delete the object that represent our state machine.
** Internal activities
One state can react to event without having a transition by declaring the
event guard and activity inside the state. This is similar to a *self-transition*.
** Activity states
You can have state that do some work without having to take a transition + activity.
You need to mark it with the "do:/" keyword inside the state. A "do:/" activity
can take some time and can be interrupted while the *internal activities* take no
time and cannot be interrupted.

* Exercise
** Component design
Different representation of the component.
- One component diagram where you show ports and connector to ports here no
  interfaces used or implemented
- Second diagram maybe focusing on the interfaces of two components where I can
  see which interfaces are been implemented. We need to show what kind of
  methods they have, not just the lollipop connector.
- We need protocol state machine for the interfaces *[pre]* /event/ *[post]*, we need *[pre]* and *[post]*
  condition which are statements on the state of the system they don't tell
  anything about something has happen or something is going to happen. In detail
  we have a *[pre]* condition, and *operation* which must be one of the operation
  of the interface, the *[post]* condition which must state that the state is
  changed like an operation has been sent for example with the [^] called the
  /hat identifier/ for asynchronous code or just the [ ] with a condition for
  synchronous code.
- Each of the interface that are mention in the second component diagram should
  provide a protocol state machine

** Detailed class design
- All the required interfaces are used and all the provided interfaces are
  implemented [3:40]
- A component can be implemented by one class or by a set of classes [04:00]
*** Sub-component [4:25]
A way do implement component is to use sub-components here we need to pay
attention that the port of the outer component are connected to the sub
component with a solid or a broken line and it is called delegation.

** Behavior
- Each classes has to have a life-cycle state machine [5:00]
- Each transition has to have an operation an event and a guard and effect. Here the
  guard is like the precondition in the protocol state machine. Behavior the
  same but guards are a little bit different from precondition. [5:30]


- Operation :: should one of the class and it might have a parameter [6:00]
- Guard :: guard can mention the parameter from the operation [6:08]
- Effect :: Written in some kind of programming language [1:07]
