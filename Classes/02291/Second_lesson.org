#+TITLE: Second lesson
* Activities in software development.

We start with the documentation of what kind of the software the customer wants.
So we start with an analysis,
Then we do a design in order to see if our idea of implementation works.
After we do and build the software, and finally we test the software.

** Two types of requirements
*** User requirements
What the user wants in their applications, page 4, these requirements are very
difficult to implement. Because it is very unclear, system requirements in the
other way adds all the details the user left out in its requirements. From the
user requirement to the system requirements is called *requirement analysis*.
You should not invent the requirements, the designer asked the user what he
wanted, and what kind of operations.

** Travel agency
These represent the user requirements

** Types of requirements
We use-case to describe requirements. Requirement can be part of the solution
but they can be part of the problem.
- Function requirements :: how the system works, from the input what is the
     output
- non functional requirements :: choice of the operating systems, programming
     language. Sometimes are solution and sometimes are requirement.

** Categories of non functional requirements
This is a list of non-functional requirements. Software is globally or just in a
country so you have to look at legal restrictions present in all the countries.
*** Organizational environment
Is software running in the cloud? or where
*** Product requirements
*** Usability requirements
How do you mesure the success.

** Good requirements
all the requirements should be testable, but how to check the *usability*, so
every time you think about requirements you should think about the tests. For
this reason a requirements should be *measured* in order to create a test.
*** Example
User survey, but what would you ask in the user survey? ask if it is easy to
use? you impose that 90% of the survey is positive.
What about user errors? The error rate per hour is smaller than 6. Easy to use
for medical staff?
*** Example requirements
You can do survey end other things to

** Possible measurement
Attach this kind of thest to the *non-functional* requirement, so this
requirements are only useful if you can test these requirements. The worst thing
that can happen is that the client says that the software is unusable
** TODO Requirements engineering process
- Ask the difference between agile and this
Is like a feedback loop, you should iterate every time to refine your
requirements, this slide predate the Agile software development. The steps here
are the isesntation spte, means that find requirement, , validation requirement
means that the requirements are consistent or can i build this system?

- Business requirements specification,
- Feasibility study means look at the business requirements, project is too large?
  Does really give the benefit that should think
- User requirements
- User requirements specifications writing them down
- Prototype, all the requirements in software or just a design
- System request elicitation, this helps to redefine the requirements
- System requirements specification and modeling use cases diagram
- Reviews
- Documentation

In agile is more *prototyping*, you can fix the requirements before writing
down the software,
** Requirements engineering
Ask question to the people, what requirements they want, the domain model is a
*class* diagram or *activity diagram* or *workflow*, can you improove the
workflow that already exist. Uses-cases and user story are very similar and
they specifies what the user wants to do with the software. Compose by the
domain model that uses the two previous diagram. Use cases and user stories are
the same in our context.
- Validation
  - Reviews
  - Test generation, can I formulate a test and if you can't you revisit the
    requirements and then prototyping.

** How to document slide 12
Four things
- How to document requirements,
  - Diagrams
  - Domain  model class diagram, activity diagrams and the one present in the slide
** Requirements issue
The developer more often add unnecessary requirements.
*Agile methods want to customer on site*. The system is always in evolution and
can change very fast.

* Domain model
Capture the customer's knowledge, what is the problem it doesn't have anitingh
to do with the solution. It is about the lenguage that we use to explain the
concepts of the problem, travel agency  what is a tirp a travel what kind of
separation I need.
- You want to understend thier problem and find a way to communicate with the
  client in an non computer scientis
- You don't know what is happening in that domain, so underst of what are the
  concepts a glossary to know the word and their explanantion, class diagram to
  find how the concepts are related, and sometimes there are the business
  process for the UML activity.

** Glossary
Is just a list of term, you can go far-far away with this, but you should focus
only on the term related to you project.

*** Example
Here book is the abstract record of the book, basically the metainformation of
what author of the book end other things. A copy is the physical copy. You need
to be prepare and understand the language of the domain, it may not bee that
trivial. So it is important to record all the vocabulary.

** Domain models terms and relations.
How things are related, it very small but it show how the library works. It is
not enought on its own but it still a nice overviews of the system, but you need
the glossary. This can form the *basis* of an implementation. Borrow is the
name of the association, ( this not really common in other diagram, here you see
role name far to the right) you need to distinguish the name and the role of the
association. The arrow means that even if the order is in one directions,
library borrows copy and not borrows copy library. *Reading* direction. Here
we don't use attributes, not because their are prohibit, but it is better to
write them in a way like at right of the slide. The reason is abstraction, title
might be not a string so it is hard to model in a precise way in the domain
diagram in a so early time of the modeling.

** Activity diagram: Business process
If we can detect what are the workflow in a given problem domain, so we can use those for a
starting point in modeling. Human interaction with the system that define the
requirements. From each box we can define the various requirements that need a
particular step. These doesn't mean that we have to build all the box, this is
just an overview of the system and which part are involved in the system.
If you have these business process you are lucky because your implementation
would be easier. In reality this process doesn't exist or their are just a talk.

* Uses Cases
** Definition
Use case is an interaction of one o more actors with the system to achieve a
specific goal. The use case is the round box and the actor is the customer, the
big box is the system. The use case describe the interaction with a system. The
system is very important and must not be implicit. So you can understand where
are the system bundries. The use case is a verb plus a noun, the goal of use
case is to achieve somethings.
- Interactions
  - Anything the actor does with the systems
  - Effects to the customer, the effect would return to the trip and the trip
    was safe and return back the user impression of the trip.

We should avoid how the system works unless is part of the Effects and
impression, so unless is not required in the last previous point.

** Use Case
Two way of showing it, but thy must describe the *same thing*, *names* need to
be the same
- Diagram

- Text description

*** TODO Diagram
We have an *actors*, and an actor can have one or more use-case, their are always
out of the systems. They are most often *person* that interact, but they can
also been *systems*, for example the *accounting system*.
We have relations ship between *use-case* not recomended to use it now or only
if you have everything clear in your mind. There are *sub use-case* if they are
used by other use-cases and the *include* arrow. This is a *special class*, like
the word *include* is a dependency. The actor is a class, use-cases is a class
and has the special type of class, the line from actor to use-case are
associations that *don't use arrows*, that is wrong.
*** Next slide
There are also extensions of use-cases next slide is Place conference call
important and you want to make it visible? while you making a call you can also
make a conference call and not all the phone can make a conference call so there
is an *option*. Ore you have to deal with two simultaneous call.
*** Summary
To sum up we have include extend and generalization. Use-case are class so can
have relationship one use-case have multiple choice the system will choose
retinal scan or password checking slide 26. A use case diagram doesn't explain
how the interaction works.

*** Generalization between actors slide 28
Commercial customer can have multiple use-case than a general customer, but
commercial customer can inherits use-case from customer.

** Type of use cases

We can have different abstractions of use cases.  Detailed use cases only for
the lowest level.
*** Example 30
*** Example 31
Lower level of abstraction. You need to find the level of abstraction that you
are confident with.
** Don't of use cases
Describe a scenario, how you are interacting with the system, this is not the
correct solution. The actor has to connect with all the use-cases and not the
use case connect to each other

The second explain how it works and this is not the role of the use-case, its
role is explaining the use cases.

The third is the correct ways.

** System boundary
We view the MUD game as everything, phone world ext...
*** Refinements 34
Phone became an actor, or Game server became the system and the phone became an
actor. You can use this as design methodology. So you restructure your system
and you can get more use-case diagram to show the various requirements, for
example we define the requirements for the game server. In this course we are
talking only to high level components.

** Detailed use case.
The detailed use case has a structure, there are different types. These are ours

You have
- Name
- Description
- actor
- main scenario path interaction to the system to achieve a specific goal how the
  user and system react. Not doing any user interface modeling. Use case should
  not tell user interface definition unless it is very important and is a
  requirement. Leave the user interface out. Interactions actor system no UI.
- alternative scenario
  - other way to achieve the goal, password or NEMID, success scenarios are both.
  - what happen if something goes wrong, data is not correct or symbols not ASCII.
- notes: the input data or *non* functional requirements.

Extended case would a be a detailed use case on its own, it could be part of the
alternative scenario. But for conference call better a detailed scenario.

** Detailed use case: Template

We should stick with this.
Precondition is not mandatory, also the post-condition is not mandatory
*** Precondition tips
The issue here is for the deleted trip, each time to delete the trip you have to
login. You can use a precondition that specify you should login.
So delete login user from main scenario in the right slide 37.
** Very important
Use case name are the same of the detailed use case, how the interaction works
in the detailed use-case. If you have an extend relation in the use-case diagram
you should be able to describe it and see it in the detailed diagram. Last point
very important.

** Use case benefit
The purpose of them are to present the functional requirements, you can capture
non-functional requirements in the notes of detailed, You can check the
completeness, one use-case diagram that show all the. Use case diagram show you
the functionality of the systems. Also the detailed description you have to
specifies the interactions and what happen in the correct case and in the fail
cases. Here you can find the things that you have miss in the use case diagram.
Test cases can be derived from the scenario of the use case, each scenario is
vailabel test
** Limitations

Not good for capturing non interaction base requirements. They should not
contain any user specific aspects. Sometimes are easier to develop a system with
the UI in mind but this are not part of the use case diagram and the detailed description.



* Use Cases                                                            :Book:

A use case specifies the behavior of a subject system or part of a system it
describes sequences of actions, including variants. A use case is a description
of a set of sequences of actions, that a subject performs to yield an observable
result of value to an actor. A use case does something that has a value for the actor.

** Subject
Is a class described by a set of use cases. Usually the class is a system or
subsystem. The use cases represent aspects of the behavior of the class.
** Names
Every use case must have a name that distinguish it from other use cases. And
composed as name + actions.

** Use case and actors
An actor represents a coherent set of roles that users of use cases play when
interacting with these use cases. An actor can be an human a device or a system
which interact with a system. Actors may be connected to use cases only by
association. An association between an actor and use case indicates that the
actor and the use case communicate with one another, each one possibly sending
and receiving messages.

** Use Cases and Flow of Events
A use case describes what a system does but it does not specify how ti does it.
You can specify the behavior of a use case by describing a flow of events in text
clearly enough for an outsider to understand it easily.


** Use case Hint and tips
- Names a single, identifiable, and reasonably atomic behavior of the system or
  part of the system
- Factors common behavior by pulling such behavior from other use cases that it includes
- Factors variants by pushing such behavior into other use cases that extend it
- Describes the flow of events clearly enough for an outsider to easily
  understand it.
- Is described by a minimal set of scenarios that specify the normal and variant
  semantics of the use case.
** Use Case Diagrams
- Need to specify the actors and meaning of their roles
- Specify what a subject should do, independently from /how/ a subject should do
  it, view the entire subject as a black box.

- Identify the boundaries of a system by deciding which behaviors are part of it
  and which are performed by external entities. This defines the subject.

- Identify the actors that surround the system by considering which groups
  require help from the system to perform the tasks.

To model the requirements of a system
- Establish the context of the system by identifying the actors that surround it.
- For each actor, consider the behavior that each expects or requirements the
  system provide
- Name these common behavior as use case
- Factor common behavior into new use cases that are used by others, factor
  variant behavior into new use cases that extend more main line flows
- Model these use cases, actors, and their relationships in a use case diagram.
- Adorn these use cases with notes or constraints that assert nonfunctional
  requirements; you may have to attach some of these to the whole system

A use case diagram can be *Forwarded Engineering* to form test for the element
to which it applies. For each use case in a use case diagram, you can create a
test case that you can run every time you release a new version of that element.

** Hints and tips
Use case diagram are a static representation of the use case view of the system.
Collectively, all the use case diagrams of a system represent the system's
complete static use case view. Individually each represent just one aspect.

- Focused on communicating one aspect of a system's static case view
- Contains only those use cases and actors that are essential to understanding
  that aspects.
- Provides detail consistent with its level of abstraction.
- Is not so minimalist as to misinform the reader about semantics that are important.
- Give it a name that communicate its purpose.
- elements that behaviors are semantically close are laid out physically close.
