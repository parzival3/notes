* Task per il test del DHCP
** DONE Flashare nuovo dhcp su tutte le board
   CLOSED: [2018-11-30 ven 10:20]
Ho flashato il primo tablet 7in e verificato che andasse in local link automaticamente
Il comando che ho usato per filtre i log e'
#+BEGIN_SRC sh
adb logcat -v time dhcpcd:D EthernetNetworkFactory:D NetUtils:D EthernetServiceImpl:D ConnectivityService:D \*:S
#+END_SRC
Ho flashato tutti e due i dispositvi
** DONE Installazione delle applicazioni [3/3] [100%]
   CLOSED: [2018-11-30 ven 10:44]
- [X] ~zeroconf-browser~ Ho trovato questo [[https://apkpure.com/zeroconf-browser/com.melloware.zeroconf][qui]]
- [X] ~iperf~, usato [[https://apkpure.com/it/iperf-for-android/com.magicandroidapps.iperf][iperf]]
- [X] ~by-cloud~

** DONE Configurazione server DHCP
   CLOSED: [2018-11-30 ven 11:27]
Il server dhcp deve essere configurato con un lease time di 2min
Ho configurato il server dhcp con questi valori e lease time di *120* secondi
#+BEGIN_SRC sh
ddns-update-style none;

default-lease-time 120;
max-lease-time 120;

authoritative;

log-facility local7;

subnet 192.168.1.0 netmask 255.255.255.0 {
  range 192.168.1.100 192.168.1.200;
  option domain-name-servers ubuntu.server.vimar.dave;
  option domain-name "dave.vimar.subnet";
  option subnet-mask 255.255.255.0;
  option routers 192.168.1.254;
  option broadcast-address 192.168.1.255;
  default-lease-time 120;
  max-lease-time 120;
}
#+END_SRC

Ho dovuto cambiare switch perche' altrimenti non andava.

Testato con i comandi
#+BEGIN_SRC sh
iperf -s -p 5201 -n 1024M
#+END_SRC
e
#+BEGIN_SRC sh
iperf -c 192.168.1.121 -p 5201
#+END_SRC


** DONE Configurare hostname di tutti gli MTSx 
   CLOSED: [2018-11-30 ven 11:51]
L'applicazione non funziona in quanto non riesce a connettersi al server. Quindi non sono bloccato.

A quanto pare non server attivare l'applicazione per fare in modo che mandi i messaggi.

** DONE Test da eseguire
   CLOSED: [2018-12-21 ven 10:01]
Per testare la comunicazione tra due MTS viene usata la APP "iperf" su entrambi gli MTS, su uno configurata in modalità
server, sull'altro in modalità client.
#+BEGIN_SRC 
MTS 1 (192.168.1.121)   –  SERVER
Comando: iperf -s -p 5201
MTS 1 (192.168.1.121)   –  CLIENT
Comando: iperf -c 192.168.1.121 -p 5201
#+END_SRC
*** DONE Test base
    CLOSED: [2018-11-30 ven 11:51]
Test con *dhcp* con tutte le schede nel dominio ~192.168.1.*~
Testo con successo con leas time 2 min.
*** DONE Avvio senza router
    CLOSED: [2018-11-30 ven 12:12]
Ho verificato che partendo senza route i dispositivi fossero configurati nella sottorete ~192.168.0~ e che *iperf* e
*zeroconf* vedano correttamente gli indirizzi. Fatto gli screenshots
*** TODO Perdita internet
Ho aumentato il lease time portandolo a 4min, ho fatto partire prima mts7 e quando era completamente acceso il 7 ho 
fatto partire mts4. Qunado quest ultimo aveva finito il boot ho rimosso il server *dhcp* e mandato in polling il
comando ip dal mts4 sull'indirizzo ~192.168.1.102~ corrispondente al 7. La prima cosa strana che ho notato e' che quando
il 7 si e' riconfigurato in local link, cercando di pingare il nuovo indirizzo il comando ping mi restituiva 
*destination unrecheable*, quindi ho aspettato fiche' anche mts4 andasse in local link e provato ad eseguire il ping
nuovamente, in questo caso la risposta e' stata
#+BEGIN_SRC sh
connect: Network is unreachable
#+END_SRC

Insospettito ho provato a riavviare il server *dhcp* dopo un minuto circa i due dispositivi erano configurati in *dhcp*
senza alcun problema.
*** DONE Ripristino router
    CLOSED: [2018-11-30 ven 12:21]
Testato partendo dalla condizione *TEST3*, i dispositivi prendono l'indirizzo ~192.168.0.*~ al prossimo annuce dopo la
riattivazione del server *DHCP*. Ho fatto gli screenshots e verificato che su *zeroconf* entrambi i dispositivi connessi
fossero visualizzati. E' difficile testare la condizione di passaggio perche' i due dispositivi si riconnetto quasi 
istantaneamente.
*** DONE Disconnessione MTSx 1
    CLOSED: [2018-11-30 ven 12:29]
Partendo dalla situazione del *TEST4*, ho disconnesso *MTS7* e verificato che in *zeroconf* non veniva visto nessun
servizio per il 7 e il 4 mostrava solo se stesso. In questa situazione *iperf* mostra ancora l'indirizzo ip dato dal
server per *MTS7* ma facendo
#+BEGIN_SRC sh
root@mtsx:/ # ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN 
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: can0: <NOARP,ECHO> mtu 16 qdisc noop state DOWN qlen 10
    link/can 
3: eth0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc pfifo_fast state DOWN qlen 1000
    link/ether 00:50:c2:1e:af:ea brd ff:ff:ff:ff:ff:ff
    inet6 fe80::250:c2ff:fe1e:afea/64 scope link 
       valid_lft forever preferred_lft forever
4: sit0@NONE: <NOARP> mtu 1480 qdisc noop state DOWN 
    link/sit 0.0.0.0 brd 0.0.0.0
#+END_SRC
non e' presente nessun idirizzo per l'interfaccia eth0

** Note varie

Il perche' non funziona il passaggio da *ipv4ll->dhcp* e' da attribuire al fatto che il framework o il kernel non
fa il flush delle tabelle di route dell'interfaccia *eht0*. Infatti esistono piu' tabelle di routing la *main* la quale
dumpiamo i risultati con il comando ~route~. Questa pero' non e' l'unica, infatti quando usiamo il comando 
~ip route flush cache~ andiamo a =flushare= solo questa tabella. Android pero' necessita di regole complesse per la
gestione delle varie applicazioni e del networking associato ad esse, per questo motivo usa in maniera pesante
*iptables* e le sue regole. In queste regole ce ne' una la quale marca tutti i pacchetti apartenenti allo user
*dhcp* sulla interfaccia *eht0* (almeno cosi' ho capito).
#+BEGIN_SRC sh
root@mtsx:/ # ip rule
0:      from all lookup local 
10000:  from all fwmark 0xc0000/0xd0000 lookup legacy_system 
10500:  from all oif eth0 uidrange 0-0 lookup eth0 
13000:  from all fwmark 0x10063/0x1ffff lookup local_network 
13000:  from all fwmark 0x10069/0x1ffff lookup eth0 
14000:  from all oif eth0 lookup eth0 
15000:  from all fwmark 0x0/0x10000 lookup legacy_system 
16000:  from all fwmark 0x0/0x10000 lookup legacy_network 
17000:  from all fwmark 0x0/0x10000 lookup local_network 
19000:  from all fwmark 0x69/0x1ffff lookup eth0 
22000:  from all fwmark 0x0/0xffff lookup eth0 
23000:  from all fwmark 0x0/0xffff uidrange 0-0 lookup main 
32000:  from all unreachable
#+END_SRC

Per capirlo abbiamo visto che il comando
#+BEGIN_SRC sh
root@mtsx:/ # ip route get 192.168.10.0/24                                    
192.168.10.0 via 192.168.1.254 dev eth0  src 192.168.1.101 
    cache 
#+END_SRC

Ora bisogna capire chi e' che inserisce quelle regole se il framework o se sono inserite nel kernel.

** Redo task MTSx
*** DONE Configurazione board
    CLOSED: [2018-12-04 mar 17:39]
*** DONE TEST1 "OK"
    CLOSED: [2018-12-04 mar 17:39]
*** DONE TEST2 "perdita internet"
    CLOSED: [2018-12-04 mar 17:39]
*** DONE TEST3 "avvio senza router"
    CLOSED: [2018-12-04 mar 17:39]
Testato correttamente con screenshots.
*** DONE TEST4 "ripristiono router"
    CLOSED: [2018-12-04 mar 17:39]
Testato partendo dalla situazione del *TEST2*
*** DONE TEST5 "dsconnessione MTSx1"
    CLOSED: [2018-12-04 mar 17:39]

** Test con build user
*** Reboot quando il dispositivo va in suspend to ram.
Ho notato che all'avvio del dispositivo flashato con full wipe e senza certificati, il dispositivo va normalmente in
suspend to ram. Qeusto comportamento non era stato notato durante lo sviluppo perhce' avevo abilitato il always on
delle developer mods. Inoltre in questa modalita' il dispositivo non puo' funzionare perche' non e' possibile pingare
il device e di coseguenza lo sviluppo fatto per il *dhcp* e' inutile.
*** DONE Intallato le due applicazioni ed avviato *by-cloud*
*** DONE TEST1 partenza con server *dhcp*
    CLOSED: [2018-12-05 mer 11:02]
- [X] ping
- [X] iperf
- [X] zeroconf

*** DONE TEST2 perdita router
    CLOSED: [2018-12-05 mer 11:06]
- [X] ping
- [X] iperf
- [X] zeroconf

    CLOSED: [2018-12-05 mer 10:40]
*** DONE TEST3 avvio senza server *dhcp*
    CLOSED: [2018-12-05 mer 10:49]
- [X] ping eseguito con sucesso
- [X] iperf
- [X] zeroconf
*** DONE TEST4 ripristino router
    CLOSED: [2018-12-05 mer 10:51]
Come gia' spiegato la configurazione di transizione e' molto difficile da provare.
- [X] ping
- [X] iperf
- [X] zeroconf

Ho notato che e' l'app zeroconf aggiorna automaticamente il servizio senza doverla chiudere forzatamente.

*** DONE TEST5 disconnessione MTS1
    CLOSED: [2018-12-05 mer 10:56]
- [X] ping
- [X] iperf
- [X] zeroconf

No l'applicazione zeroconf non sembra aggiornarsi automaticamente ma bisogna andare nel menu' delle applicazioni
recenti e ricliccare su zeroconf per aggiornare la lista dei dispositivi.

* Task nuovo test DHCP dopo le modifiche
Dato che il framework continuava a crashare dopo aver inserito
la patch per il DHCP, ho dovuto modificare dinuovo il framework
usando il seguente [[http://letslearnjavaj2ee.blogspot.com/2013/08/timer-and-timertask-task-already.html][link]]. Quello che ho fatto e' stato cambiare la
variabile contenente il TimerTask con una classe vera e propria in
modo da distruggerla e crearla quando si va a schedulare o rimuovere
un nuovo timer.
** Test con build eng
*** Installazione applicazioni
Ho trovato questo semplice comando per installare le varie applicazioni
in piu' device contemporaneamente
#+BEGIN_SRC sh
adb devices | tail -n +2 | cut -sf 1 | xargs -iX adb -s X install ...
#+END_SRC
*** Test funzionamento *iperf*
Passato entrambi i dispositivi avevano lo stesso mac addres
*** DONE TEST1 "OK"....
    CLOSED: [2018-12-21 ven 15:41]
*** DONE TEST2 "perdita internet"
    CLOSED: [2018-12-21 ven 16:02]
Non sono sicurissimo che funzioni sempre
*** DONE TEST 3 "avvio senza router"
    CLOSED: [2018-12-21 ven 16:08]
*** DONE TEST 4 "ripristino router"
    CLOSED: [2018-12-21 ven 16:11]




