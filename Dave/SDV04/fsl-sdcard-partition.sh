#!/bin/bash

# android-tools-fsutils should be installed as
# "sudo apt-get install android-tools-fsutils"

# partition size in MB
BOOTLOAD_RESERVE=8
BOOT_ROM_SIZE=16
SYSTEM_ROM_SIZE=800
CACHE_SIZE=512
RECOVERY_ROM_SIZE=16
DEVICE_SIZE=8
MISC_SIZE=6
DATAFOOTER_SIZE=2

# if pv exists, is useful to show progress during remote flashing
command -v pv && PV="pv -t -r -e -p -b "

NOTIFICATION_IMAGE=/tmp/msg.png

help() {

bn=`basename $0`
cat << EOF
usage $bn <option> device_node

options:
  -h				displays this help message
  -s				only get partition size
  -np 				not partition.
  -f soc_name			flash android image.
  -c uniqueid			system uniqueid.
  -r remote-target	remote target to program (e.g. root@192.168.0.x)
EOF

}

function display_notify()
{
	CONVERT=$(remote_command command -v convert)
	FBI=$(remote_command command -v fbi)
	if [ -n "$CONVERT" ] && [ -n "$FBI" ]
	then
		# both commands exist. We can generate image and display them on FB
		remote_command killall fbi
		remote_command rm -f msg.png
		remote_command "echo $* | convert -background black -fill white -gravity center -pointsize 24 \
			-font DejaVu-Sans-Mono -pointsize 40 \( label:@- -rotate $CONVERT_ROTATE \) $NOTIFICATION_IMAGE"
		remote_command fbi -noverbose -T 1 -d /dev/fb0 $NOTIFICATION_IMAGE
	fi
}

function notify()
{
	echo $* 2>&1 | tee -a ${update_log}
	display_notify $*
}

function error()
{
	notify $*
	while [ 1 ]
	do
		sleep 5
	done
}

function remote_command()
{
	if [ "${remote_target}" -eq "1" ]
	then
		ssh ${target} "export PATH=/usr/sbin:/usr/bin:$PATH; $*"
	else
		eval $* 2>&1 | tee -a ${update_log}
		return ${PIPESTATUS[0]}
	fi
}

function remote_dd()
{
	if [ -n "$PV" ]
	then
		# get source file name
		filename=$(echo $1 | awk -F= '{print $2}')
		# get its size if its exists
		if [ -f $filename ]; then
			filesize=$(stat --printf="%s" $filename)
		else
			filesize=0
		fi
		echo "Estimated file size of $filename is $filesize"
		dd $1 | $PV -s $filesize | remote_command dd ${@:2}
	else
		dd $1 | remote_command dd ${@:2}
	fi
}

STARTTIME=$(date +%s)

# parse command line
moreoptions=1
node="na"
soc_name=""
uuid=""
cal_only=0
flash_images=0
not_partition=0
not_format_fs=0
bootloader_file="u-boot.imx"
bootimage_file="boot.img"
systemimage_file="system.img"
systemimage_raw_file="system_raw.img"
recoveryimage_file="recovery.img"
remote_target=0
while [ "$moreoptions" = 1 -a $# -gt 0 ]; do
	case $1 in
	    -h) help; exit ;;
	    -s) cal_only=1 ;;
	    -f) flash_images=1 ; soc_name=$2; shift;;
	    -c) uuid=$2; shift;;
	    -np) not_partition=1 ;;
	    -nf) not_format_fs=1 ;;
	    -r) remote_target=1; target=$2; shift;;
	    *)  moreoptions=0; node=$1 ;;
	esac
	[ "$moreoptions" = 0 ] && [ $# -gt 1 ] && help && exit
	[ "$moreoptions" = 1 ] && shift
done

SCRIPTPATH=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
logfiles=`ls ${SCRIPTPATH}/ | grep ${uuid}_update | wc -l`
update_log="${SCRIPTPATH}/${uuid}_update_${logfiles}.log"

# check remote connection and device
if [ "${remote_target}" -eq "1" ]
then
	echo "Checking remote connection to '${target}'"
	remote_command uname -a

	remote_command test -b ${node}
	if [ ! "$?"  ]
	then
		echo "Cannot find ${node}"
		help
		exit
	fi
else
	# if working locally its easier!
	if [ ! -b ${node} ]; then
		help
		exit
	fi
fi

# no need for image rotation on GSD
CONVERT_ROTATE="0"

# format the SDCARD/DATA/CACHE partition
part=""
echo ${node} | grep mmcblk > /dev/null
if [ "$?" -eq "0" ]; then
        part="p"
fi

# call sfdisk to create partition table
# get total card size
seprate=40
total_size=`remote_command sfdisk -s ${node}`
total_size=`expr ${total_size} / 1024`
boot_rom_sizeb=`expr ${BOOT_ROM_SIZE} + ${BOOTLOAD_RESERVE}`
extend_size=`expr ${SYSTEM_ROM_SIZE} + ${CACHE_SIZE} + ${DEVICE_SIZE} + ${MISC_SIZE} + ${DATAFOOTER_SIZE} + ${seprate}`
data_size=`expr ${total_size} - ${boot_rom_sizeb} - ${RECOVERY_ROM_SIZE} - ${extend_size}`

# create partitions
if [ "${cal_only}" -eq "1" ]; then
cat << EOF
BOOT   : ${boot_rom_sizeb}MB
RECOVERY: ${RECOVERY_ROM_SIZE}MB
SYSTEM : ${SYSTEM_ROM_SIZE}MB
CACHE  : ${CACHE_SIZE}MB
DATA   : ${data_size}MB
MISC   : ${MISC_SIZE}MB
DEVICE : ${DEVICE_SIZE}MB
DATAFOOTER : ${DATAFOOTER_SIZE}MB
EOF
exit
fi

# Check MD5 sum of update binaries before flashing into the device
MD5SUM_file="md5sum.md5"
if [ -e ${MD5SUM_file} ]; then
    notify "Checking MD5..."
    remote_command md5sum -c ${MD5SUM_file}
    if [ "$?" -eq "0" ]; then
        notify "Files checksum OK"
    else
        error "Files checksum WRONG"
    fi
fi

function format_android
{
    notify "formatting data"
    remote_command mkfs.ext4 -F ${node}${part}4 -L data
    notify "formatting system"
    remote_command mkfs.ext4 -F ${node}${part}5 -Lsystem
    notify "formatting cache"
    remote_command mkfs.ext4 -F ${node}${part}6 -Lcache
    notify "formatting device"
    remote_command mkfs.ext4 -F ${node}${part}7 -Ldevice
}

function flash_android
{
	bootloader_file="u-boot.imx"
	bootimage_file="boot-${soc_name}.img"
	recoveryimage_file="recovery-${soc_name}.img"
if [ "${flash_images}" -eq "1" ]; then
    echo "flashing android images..."
    echo "bootloader: ${bootloader_file}"
    echo "boot image: ${bootimage_file}"
    echo "recovery image: ${recoveryimage_file}"
    echo "system image: ${systemimage_file}"
    remote_dd if=/dev/zero of=${node} bs=1k seek=384 count=129
    notify "Flashing bootloader"
    remote_dd if=${bootloader_file} of=${node} bs=1k seek=1
    notify "Flashing boot.img"
    remote_dd if=${bootimage_file} of=${node}${part}1
    notify "Flashing recovery"
    remote_dd if=${recoveryimage_file} of=${node}${part}2
    notify "Checking system image"
    md5=$(md5sum $systemimage_file | cut -f 1 -d ' ')
    if [ ! -e ${systemimage_raw_file}_${md5} ]; then
        notify "Preparing system image"
        simg2img ${systemimage_file} ${systemimage_raw_file}_${md5}
    fi
    notify "Flashing system image"
    remote_dd if=${systemimage_raw_file}_${md5} of=${node}${part}5
    remote_command sync
fi
}

function update_environment
{
    # configure for environment in eMMC
	echo "${node} 0x100000 0x2000" > /etc/fw_env.config

    # check if we can read current environment
    # if not stop before compromising it
    fw_printenv bootcmd 2>&1 | grep "Bad CRC"
    if [ "$?" -eq "0" ]
    then
        error "Error while reading environment. Aborting."
    fi

    notify "Updating environment"
    sleep 3

    # mmc_andr
    fw_setenv mmc_andr 'run addcons addmisc addandroidargs; boota ${fastboot_dev}'
    # disable selinux and remove serialno from commandline
    fw_setenv addandroidargs 'setenv bootargs ${bootargs} init=/init androidboot.console=${console} androidboot.hardware=freescale androidboot.selinux=disabled'
    # add mmc_andr on bootcmd
    fw_setenv bootcmd 'run usbrecovery; run mmcrecovery; run mmc_andr; run ${normalboot}'

}

if [[ "${not_partition}" -eq "1" && "${flash_images}" -eq "1" ]] ; then
    flash_android
    exit
fi

notify "Partitioning ${node}"

remote_command sfdisk --force ${node} << EOF
,${boot_rom_sizeb}M,83
,${RECOVERY_ROM_SIZE}M,83
,${extend_size}M,5
,${data_size}M,83
,${SYSTEM_ROM_SIZE}M,83
,${CACHE_SIZE}M,83
,${DEVICE_SIZE}M,83
,${MISC_SIZE}M,83
,${DATAFOOTER_SIZE}M,83
EOF

remote_command partprobe

# adjust the partition reserve for bootloader.
# if you don't put the uboot on same device, you can remove the BOOTLOADER_ERSERVE
# to have 8M space.
# the minimal sylinder for some card is 4M, maybe some was 8M
# just 8M for some big eMMC 's sylinder
remote_command sfdisk --force ${node} -N1 << EOF
${BOOTLOAD_RESERVE}M,${BOOT_ROM_SIZE}M,83
EOF

remote_command partprobe

format_android
flash_android
update_environment

ENDTIME=$(date +%s)

echo Elapsed time: $(($ENDTIME - $STARTTIME)) seconds

notify "Everything DONE"

# For MFGTool Notes:
# MFGTool use mksdcard-android.tar store this script
# if you want change it.
# do following:
#   tar xf mksdcard-android.sh.tar
#   vi mksdcard-android.sh 
#   [ edit want you want to change ]
#   rm mksdcard-android.sh.tar; tar cf mksdcard-android.sh.tar mksdcard-android.sh
