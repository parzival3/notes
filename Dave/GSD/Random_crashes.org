* Analisi random crashes GSD
Il cliente segnala che su due dispositivi si verificano dei crash rnadomici nei quali il framework si riavvia
oppure il tablete ri-bootstrappa da solo.

Come prima ipotesi si e' pensato ad un problema di *ram* pero' questo non puo' essere perche' un device che presenta
questo tipo di problema e' stato stressato con *sat*.

Come seconda ipostesi si pensa ad un malfunzionamento della eMMc.

** DONE Verifica eMMC
   CLOSED: [2018-12-11 mar 09:00]
Ho provato a fare dei testi andando a stressare il launcher con il comando *monkey* (e' importante l'ordine degli
argomenti)
#+BEGIN_SRC sh
monkey --pct-syskeys 0 --throttle 5  --pct-appswitch 75 -v 5000  -p  com.android.launcher > /data/media/prova.log 
#+END_SRC

Qui ho riscontrato i primi problemi
#+BEGIN_SRC sh
// CRASH: com.android.launcher (pid 1531)
// Short Msg: Native crash
// Long Msg: Native crash: Segmentation fault
// Build Label: Freescale/gsd_6dp/gsd_6dp:6.0.1/1.2.1/20180910:user/test-keys
// Build Changelist: 20180910
// Build Time: 1536599716000
// *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** ***
// Build fingerprint: 'Freescale/gsd_6dp/gsd_6dp:6.0.1/1.2.1/20180910:user/test-keys'
// Revision: '0'
// ABI: 'arm'
// pid: 1531, tid: 1531, name: ndroid.launcher  >>> com.android.launcher <<<
// signal 11 (SIGSEGV), code 1 (SEGV_MAPERR), fault addr 0xf044938c
//     r0 12d392c0  r1 12ed7fe0  r2 0000000c  r3 12e350cc
//     r4 00000000  r5 00000000  r6 f044938c  r7 12ed7f80
//     r8 12c37e40  r9 b4df6500  sl 12ed7fe0  fp 00000001
//     ip b4dc9008  sp befc4d70  lr 73401791  pc 73401790  cpsr 00010030
//
// backtrace:
//     #00 pc 73401790  /data/dalvik-cache/arm/system@framework@boot.oat (offset 0x1ece000)
//     #01 pc 7340178f  /data/dalvik-cache/arm/system@framework@boot.oat (offset 0x1ece000)
//
[ 3885.155702] binder: undelivered transaction 178298
[ 3885.160710] binder: undelivered transaction 178254
[ 3885.166051] binder: undelivered transaction 178235
[ 3885.171057] binder: undelivered transaction 178267
System appears to have crashed at event 4975 of 5000 using seed 188179675
tch 75 -v 5000 --throttle 500 > /data/media/prova.log                         <
tch 75 -v 7000 --throttle 500 > /data/media/prova.log                         <
[ 3958.676857] binder: 751: binder_alloc_buf, no vma
[ 3958.681576] binder: 206:254 transaction failed 29201, size 68-0
[ 3958.705621] binder: release 751:1465 transaction 273415 in, still active
[ 3958.712482] binder: send failed reply for transaction 273415 to 1619:1619
[ 3958.719507] binder: undelivered transaction 273539
[ 3958.724347] binder: undelivered transaction 273544
[ 3958.730194] binder: send failed reply for transaction 273542 to 1586:1586
[ 3958.738313] binder: 1619:1619 transaction failed 29189, size 416-0
Failed talking with activity m[ 3958.747988] binder: 1619:1619 transaction failed 29189, size 68-0
anager!
Error: RemoteException while injecting event.
System appears to have crashed at e[ 3958.762950] binder: 206:255 transaction failed 29189, size 68-0
vent 706 of 7000 using seed 18827[ 3958.770472] init: Starting service 'bootanim'...
6353
194|root@gsd_6dp:/ # [ 3959.235123] init: Service 'zygote' (pid 522) killed by signal 9
[ 3959.241077] init: Service 'zygote' (pid 522) killing any children in process group
[ 3959.284548] init: write_file: Unable to open '/sys/android_power/request_state': No such file or directory
[ 3959.294285] init: write_file: Unable to write to '/sys/power/state': Invalid argument
[ 3959.316992] init: Service 'media' is being killed...
[ 3959.322561] init: Service 'netd' is being killed...
[ 3959.330751] init: Service 'netd' (pid 520) killed by signal 9
[ 3959.336988] init: Service 'netd' (pid 520) killing any children in process group
[ 3959.348169] init: Service 'media' (pid 521) killed by signal 9
[ 3959.354045] init: Service 'media' (pid 521) killing any children in process group
[ 3959.362191] init: Untracked pid 1240 killed by signal 9
[ 3959.371698] init: Service 'debuggerd' (pid 212) killed by signal 9
[ 3959.378061] init: Service 'debuggerd' (pid 212) killing any children in process group
[ 3959.386365] init: Untracked pid 1586 killed by signal 9
[ 3959.391653] init: Starting service 'netd'...
[ 3959.396554] init: Starting service 'debuggerd'...
[ 3959.401741] init: Starting service 'media'...
[ 3959.415006] init: Starting service 'zygote'...
[ 3959.594484] qtaguid: iface_stat: stat_update() wlan0 not found
[ 3960.204406] qtaguid: iface_stat: stat_update() wlan0 not found
[ 3960.274222] imx-hdmi-audio imx-hdmi-audio: HDMI Video is not ready!
[ 3960.280680] imx-hdmi-audio imx-hdmi-audio: ASoC: can't open platform imx-hdmi-audio: -22
[ 3960.289205] imx-hdmi-audio imx-hdmi-audio: HDMI Video is not ready!
[ 3960.295533] imx-hdmi-audio imx-hdmi-audio: ASoC: can't open platform imx-hdmi-audio: -22
[ 3962.598112] init: Untracked pid 1360 killed by signal 9
[ 3962.610561] init: Untracked pid 1490 killed by signal 9
[ 3962.911049] init: Untracked pid 1323 killed by signal 9
[ 3962.916981] init: Untracked pid 1230 killed by signal 9
[ 3962.935338] init: Untracked pid 1151 killed by signal 9
[ 3962.955515] init: Untracked pid 1257 killed by signal 9
[ 3962.985345] init: Untracked pid 1436 killed by signal 9
[ 3963.393606] init: Untracked pid 1132 killed by signal 9
[ 3963.435597] init: Untracked pid 1396 killed by signal 9
[ 3963.520659] mtp_release
[ 3963.523623] init: Untracked pid 873 killed by signal 9
[ 3963.715199] init: Untracked pid 1125 killed by signal 9
[ 3963.729084] init: Untracked pid 1372 killed by signal 9
[ 3963.745832] init: Untracked pid 864 killed by signal 9
[ 3965.954511] F@TOUCH ilitek_i2c_fb_notifier_enable FB_BLANK_UNBLANK
[ 3967.434499] F@TOUCH ilitek_i2c_fb_notifier_enable FB_BLANK_UNBLANK
[ 3968.415421] lowmemorykiller: lowmem_shrink: convert oom_adj to oom_score_adj:
[ 3968.422594] lowmemorykiller: oom_adj 0 => oom_score_adj 0
[ 3968.428049] lowmemorykiller: oom_adj 1 => oom_score_adj 58
[ 3968.433563] lowmemorykiller: oom_adj 2 => oom_score_adj 117
[ 3968.439190] lowmemorykiller: oom_adj 3 => oom_score_adj 176
[ 3968.444821] lowmemorykiller: oom_adj 9 => oom_score_adj 529
[ 3968.450411] lowmemorykiller: oom_adj 15 => oom_score_adj 1000
[ 3973.971289] acc_open
[ 3973.973499] acc_release
[ 3974.195799] init: Starting service 'dhcpcd_eth0'...
[ 3975.144962] mtp_open
[ 3980.076661] init: Service 'bootanim' (pid 1629) exited with status 0
#+END_SRC

Da qui e' difficile capire quale e' il vero problema, una cosa che trovo interessante e' l'output
#+BEGIN_SRC sh
[ 3958.676857] binder: 751: binder_alloc_buf, no vma
#+END_SRC
Gia' visto quando stavo lavorando sulla funzione di hotplug relativa all'*HDMI*

** DONE Test su uSD
   CLOSED: [2018-12-11 mar 09:00]
Ho creato manualmente una uSD conentenente il filesystem di gsd, ora e' necessario andare a modificare la bootimage
in modo tale da aggiornare l'fstab.
Comandi usati per la creazione della uSD.
#+BEGIN_SRC sh
mkdir tmp_bootimage
cd tmp_bootimage
abootimg -x ../boot-imx6dp.img
mkdir tmp
cd tmp/
cat ../initrd.img | gunzip | cpio -vid
sed -i -e "s@k2@k0@g" fstab.freescale
cat fstab.freescale
find . | cpio --create --format='newc' | gzip > ../initrd.img
cd ../
mkimage -n 'Ramdisk Image'  -A arm -O linux -T ramdisk -C gzip -d initrd.img initrd.new.img
cd ../
abootimg --create uSD_gsd_boot.img -f tmp_bootimage/bootimg.cfg -k tmp_bootimage/zImage -r tmp_bootimage/initrd.new.img -s gsd-1.1.0_imx6dp-gsd.dtb 
sudo dd if=uSD_gsd_boot.img of=/dev/sdb1 conv=fsync
#+END_SRC

<2018-12-06 gio>
La *uSD* riporta lo stesso comportamente, questo fa pensare che il problema sia dovuto alla *RAM*, alcuni crash
significativi sono
#+BEGIN_SRC sh
[  456.240938] Bad mode in undefined instruction handler detected
[  456.246809] Internal error: Oops - bad mode: 0 [#1] PREEMPT SMP ARM
[  456.253085] Modules linked in:
[  456.256166] CPU: 0 PID: 1532 Comm: Binder_3 Not tainted 4.1.15-gsd-1.1.0 #2
[  456.263130] Hardware name: Freescale i.MX6 Quad/DualLite (Device Tree)
[  456.269660] task: d5859200 ti: daf0c000 task.ti: daf0c000
[  456.275063] PC is at 0xffff1144
[  456.278208] LR is at 0xb6c855f4
[  456.281353] pc : [<ffff1144>]    lr : [<b6c855f4>]    psr: 000f0197
[  456.281353] sp : daf0dfb0  ip : 00001274  fp : 130a7480
[  456.292832] r10: 00000004  r9 : 9eaded00  r8 : 12d80800
[  456.298060] r7 : a8c6b850  r6 : 12d81800  r5 : 130a7480  r4 : 000034ec
[  456.304588] r3 : 12ff6ac0  r2 : 130a7480  r1 : 12d81800  r0 : 000f0193
[  456.311119] Flags: nzcv  IRQs off  FIQs on  Mode ABT_32  ISA ARM  Segment user
[  456.318343] Control: 10c53c7d  Table: 32fe404a  DAC: 00000015
[...]
[...]
[  157.968068] Unable to handle kernel NULL pointer dereference at virtual address 0000004f
[  157.976185] pgd = dded4000
[  157.978901] [0000004f] *pgd=00000000
[  157.982505] Internal error: Oops: 5 [#1] PREEMPT SMP ARM
[  157.987823] Modules linked in:
[  157.990906] CPU: 0 PID: 850 Comm: putmethod.latin Not tainted 4.1.15-gsd-1.1.0 #2
[  157.998396] Hardware name: Freescale i.MX6 Quad/DualLite (Device Tree)
[  158.004930] task: dac24200 ti: da9d8000 task.ti: da9d8000
[  158.010341] PC is at do_PrefetchAbort+0x24/0x98
[  158.014879] LR is at 0x5
[  158.017423] pc : [<c010132c>]    lr : [<00000005>]    psr: 400f0193
[  158.017423] sp : da9d9f10  ip : 00000005  fp : 2ae98980
[  158.028908] r10: 2ac7e2c0  r9 : b4d76500  r8 : 10c53c7d
[  158.034135] r7 : 00261c00  r6 : ffffffff  r5 : 00000005  r4 : ffffffff
[  158.040664] r3 : b0f599c0  r2 : da9d9fb0  r1 : 80000005  r0 : 00261c00
[  158.047194] Flags: nZcv  IRQs off  FIQs on  Mode SVC_32  ISA ARM  Segment user
[  158.054418] Control: 10c53c7d  Table: 2ded404a  DAC: 00000015
[  158.060165]
[...]
[...]
[  887.867627] nf_conntrack: automatic helper assignment is deprecated and it will be removed soon. Use the iptables CT target to attach helpers instead.
[ 1000.813723] BUG: Bad page state in process RenderThread  pfn:591eb
[ 1000.820257] page:e591ad60 count:0 mapcount:0 mapping:80000000 index:0x2
[ 1000.827705] flags: 0x40000000()
[ 1000.831246] page dumped because: non-NULL mapping
[ 1000.836125] Modules linked in:
[ 1000.839273] CPU: 0 PID: 4237 Comm: RenderThread Not tainted 4.1.15-gsd-1.1.0 #2
[ 1000.846641] Hardware name: Freescale i.MX6 Quad/DualLite (Device Tree)
[ 1000.853207] [<c011021c>] (unwind_backtrace) from [<c010c1bc>] (show_stack+0x10/0x14)
[ 1000.861054] [<c010c1bc>] (show_stack) from [<c0c39454>] (dump_stack+0x84/0xc4)
[ 1000.868366] [<c0c39454>] (dump_stack) from [<c01f0950>] (bad_page+0xd0/0x120)
[ 1000.868380] [<c01f0950>] (bad_page) from [<c01f4524>] (get_page_from_freelist+0x344/0x644)
[ 1000.868389] [<c01f4524>] (get_page_from_freelist) from [<c01f4a8c>] (__alloc_pages_nodemask+0x100/0x93c)
[ 1000.868404] [<c01f4a8c>] (__alloc_pages_nodemask) from [<c08d1154>] (_DefaultAlloc+0x330/0x360)
[ 1000.868424] [<c08d1154>] (_DefaultAlloc) from [<c08cc72c>] (gckOS_AllocatePagedMemoryEx+0xac/0x164)
[ 1000.868439] [<c08cc72c>] (gckOS_AllocatePagedMemoryEx) from [<c08df5b4>] (gckVIDMEM_ConstructVirtual+0xbc/0x100)
[ 1000.868450] [<c08df5b4>] (gckVIDMEM_ConstructVirtual) from [<c08d3658>] (gckKERNEL_AllocateLinearMemory+0x1b8/0x330)
[ 1000.868463] [<c08d3658>] (gckKERNEL_AllocateLinearMemory) from [<c08d6034>] (gckKERNEL_Dispatch+0x824/0x10f8)
[ 1000.868471] [<c08d6034>] (gckKERNEL_Dispatch) from [<c08d1ff4>] (drv_ioctl+0x11c/0x2c8)
[ 1000.868487] [<c08d1ff4>] (drv_ioctl) from [<c0246708>] (do_vfs_ioctl+0x3fc/0x5fc)
[ 1000.868497] [<c0246708>] (do_vfs_ioctl) from [<c0246974>] (SyS_ioctl+0x6c/0x7c)
[ 1000.868510] [<c0246974>] (SyS_ioctl) from [<c0108280>] (ret_fast_syscall+0x0/0x3c)
[ 1000.868859] Disabling lock debugging due to kernel taint
[ 1027.105780] Internal error: Oops - undefined instruction: 0 [#1] PREEMPT SMP ARM
[ 1027.106172] Unable to handle kernel paging request at virtual address c010d08c
[ 1027.106177] pgd = dad88000
[ 1027.106184] [c010d08c] *pgd=1001141e(bad)
[ 1027.127137] Modules linked in:
[ 1027.130212] CPU: 0 PID: 3929 Comm: RenderThread Tainted: G    B           4.1.15-gsd-1.1.0 #2
[ 1027.138740] Hardware name: Freescale i.MX6 Quad/DualLite (Device Tree)
[ 1027.145270] task: dabb0600 ti: db930000 task.ti: db930000
[ 1027.150678] PC is at do_DataAbort+0x2c/0xb4
[ 1027.154863] LR is at 0xd
[ 1027.157401] pc : [<c0101280>]    lr : [<0000000d>]    psr: 000f0193
[ 1027.157401] sp : db931170  ip : c130d4fc  fp : 00000000
[ 1027.168879] r10: db930000  r9 : c010cd6c  r8 : 00000000
[ 1027.174106] r7 : c010d08c  r6 : c0116728  r5 : 0000080d  r4 : 0000000d
[ 1027.180635] r3 : c010cd60  r2 : db931210  r1 : 0000080d  r0 : c010d08c
[ 1027.187164] Flags: nzcv  IRQs off  FIQs on  Mode SVC_32  ISA ARM  Segment user
[ 1027.194390] Control: 10c53c7d  Table: 250b004a  DAC: 00000015
[ 1027.200137]
#+END_SRC

Il comando utilizzato per fare i test piu' stressanti e'
#+BEGIN_SRC  sh
monkey --pct-syskeys 0 --pct-appswitch 75 -v 5000  -p  com.android.launcher > /data/media/prova.log 
#+END_SRC

Con il parametro *-v* che varia la quantita' di tocchi eseguiti durante il test.

** DONE Test acceleratore grafico
   CLOSED: [2018-12-11 mar 09:00]

Si e' deciso di condurre dei test sull'acceleratore grafico perche' molto probabilmente e' lui che cosuma troppo e
quindi la board si spegne senza la presenza di **oops* del kernel.
#+BEGIN_SRC sh
mkdir -p tmp_bootimage
cd tmp_bootimage
abootimg -x ../boot-imx6dp.img
cd ../linux-2.6-imx
source /home/enrico/axel/Sdk/environment-setup-cortexa9hf-neon-poky-linux-gnueabi
make imx_v7_gsd_defconfig -j9
sed -i -e "442s@[0-9]\+@100000000@g" arch/arm/boot/dts/imx6qdl-gsd.dtsi
sed -n -e 442p arch/arm/boot/dts/imx6qdl-gsd.dtsi
sed -i -e "419s@[0-9]\+@100000000@g" arch/arm/boot/dts/imx6qdl-gsd.dtsi
sed -n -e 419p arch/arm/boot/dts/imx6qdl-gsd.dtsi
make imx6qdl-gsd.dtb -j9
cp arch/arm/boot/dts/imx6dp-gsd.dtb ../
cd ../
abootimg --create mod_lvds_gsd_boot.img -f tmp_bootimage/bootimg.cfg -k tmp_bootimage/zImage -r tmp_bootimage/initrd.img -s imx6dp-gsd.dtb
#+END_SRC
<2018-12-07 ven>
Non sono riuscito a cabiare semplicemente il refrash rate, pero' quello che sono riuscito a fare e' il cambio della
risoluzione.
*** 1920X1080
La board si avvia, la frequenza e' cabiata ed e' diventata piu' bassa gurdando ~/sys/kernel/debug/clk/clk_summary~ pero'
il refrash rate non cambia e rimane sempre a =60Hz=. In qusto caso abbiamo ridotto di circa il ~13%~ calcolando
~(1920X1080)/(1980X1200) = 0,8727~.
*** 1440X480
La board non si avvia e va in crash come per lo sviluppo dell'hot plug HDMI.

** DONE Limitazioni temepratura CPU/GPU
   CLOSED: [2018-12-11 mar 09:00]
Si pensa che questa sia dovuta a delle problematiche di alimentazione della parte CPU/GPU. In questa board infatti
i rail che alimentano la parte grafica e quall del processore sono stati connessi insieme. Questo perche' il pimic
utilizzato e' stato sviluppato solo per la serie lite di questi processori. Per la serie plus come work-aroud NXP
ha consigliato di unire i tre rail di alimentazione per riuscire a erogre al micro la corrente richiesta.
Per ovviare a questo problema si e' deciso di limitare la massima temperatura raggiungibile dalla CPU/GPU usando quanto
c'e' scritto nel [[https://wiki.dave.eu/index.php/Thermal_management_(Axel)][wiki pubblico]]. Limtare la massima temperatura sembra giocare un ruolo importante solo sulla ~GPU~
(da quello che ho capito) per questa famiglia di processori. Output di esempio e'
#+BEGIN_SRC sh
System is too hot. GPU3D will work at 3/64 clock.
#+END_SRC
Andando a limitare la *GPU* impostando una soglia pari a *60C* si ottiene una maggiore stabilita' pero' siamo incappati
in un riavvio imporvviso quando e' stato fatto ripartire il test dopo che la frequenza era stata riportata al valore
normale.


** DONE Limitazione lato CPU
   CLOSED: [2018-12-11 mar 09:00]
Si e' deciso quindi di andare a limitare la frequenza, o per lo meno tenere l'assorbimento di corrente piuttosto costante
lato CPU, e provare a lanciare i test lato GPU senza limitazioni. In questo modo, la temperatura puo' salire liberamente
ma quello che viene piu' o meno livellato sono i picchi di corrente assorbito che in questo modo sono molto piu' piccoli.

Partendo dalla pagina del [[https://wiki.dave.eu/index.php/FAQs_(Axel)][wiki pubblico]], ho cambiato il governor da *ondemand* a *userspace*, e fissato la frequenza 
a circa ~1Ghz~

Per testare il tutto ho usato uno script molto semplice
#+BEGIN_SRC sh
#!/bin/sh
adb shell "echo userspace > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor"
adb shell "echo $1 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_setspeed"
adb shell cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
adb shell cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq
adb shell "echo com.android.quicksearchbox > /data/misc/black-list.txt"
adb shell "echo com.android.soundrecorder >> /data/misc/black-list.txt"
adb shell monkey --pkg-blacklist-file /data/misc/black-list.txt \
--pct-syskeys 0 --pct-appswitch 75 -v 240000  --ignore-crashes \
--ignore-timeouts --ignore-security-exceptions --pct-nav 70 \
--pct-majornav 15 --pct-touch 5 --pct-trackball 5 --throttle 5 >  monkey.log
#+END_SRC

con frequenze disponibili *396000 792000 996000*.
Se lo script si blocca perche' qualche' applicazione crasha e' sufficiente
analizzare il log monkey.log ed aggiungere l'applicazione nella black list delle applicazioni come fatto per
#+BEGIN_SRC sh
adb shell "echo com.android.soundrecorder >> /data/misc/black-list.txt"
#+END_SRC

e' possibile controllare la temperatura da un seconda shell con
#+BEGIN_SRC sh
export LC_NUMERIC=en_US.UTF-8
watch -n0.1 adb shell cat /sys/class/thermal/thermal_zone0/temp
#+END_SRC

Un comando piu' intelligente ma non ottimizzato e'
#+BEGIN_SRC sh
watch -n0.1 "adb shell cat /sys/class/thermal/thermal_zone0/temp >> temp1.txt; tail -n1 temp1.txt"
#+END_SRC

con il quale e' possibile calcolare la temperatura media tramite questo script
#+BEGIN_SRC sh
#!/bin/bash
file=$1
dos2unix $file
n_mesure=$(wc -l $file | awk '{print $1}')
echo $n_mesure
t_total=0
while read T
do
	t_total=$((T + t_total))
done < $file

echo $t_total
mean=$((t_total/n_mesure))
echo $mean
#+END_SRC
Il quale calcola la media della temperatura

E' possibile anche plottare un grafico con uno script uguale a
#+BEGIN_SRC sh
#!/usr/bin/gnuplot
#
# Creates a version of a plot, which looks nice for inclusion on web pages
#
# AUTHOR: Hagen Wierstorf

reset

# wxt
set terminal wxt size 1200,800 enhanced font 'Verdana,9' persist
# png
#set terminal pngcairo size 410,250 enhanced font 'Verdana,9'
#set output 'nice_web_plot.png'
# svg
#set terminal svg size 410,250 fname 'Verdana, Helvetica, Arial, sans-serif' \
#fsize '9' rounded dashed
#set output 'nice_web_plot.svg'

# define axis
# remove border on top and right and set color to gray
set style line 11 lc rgb '#808080' lt 1
set border 3 back ls 11
set tics nomirror
# define grid
set style line 12 lc rgb '#808080' lt 0 lw 1
set grid back ls 12

# color definitions
set style line 1 lc rgb '#8b1a0e' pt 6 ps 1 lt 1 lw 2 # --- red
set style line 2 lc rgb '#5e9c36' pt 6 ps 1 lt 1 lw 2 # --- green

set key bottom right

set xlabel 'Milliseconds'
set ylabel 'Temperature'

plot 'temp2.txt' u 0:1 t 'Example line' w lp ls 1,
#+END_SRC

Lanciando lo script con
#+BEGIN_SRC sh
gnuplot plot.sh
#+END_SRC

Ho preso spunto da qusto post [[http://www.gnuplotting.org/attractive-plots/][qui.]]

Un comando ancora piu' pro e'
#+BEGIN_SRC sh
watch -n0.1 'printf "%s\n" "$(date +"%T") $(adb shell cat /sys/class/thermal/thermal_zone0/temp)" >> new_temp.txt; tail -n1 new_temp.txt'
#+END_SRC

Dove stampo sia l'ora che la temperatura.


Se il file con la temperatura si corrompe perche' vine stampato qualche' carattere metre si sta facendo il *cat* e'
possibile usare
#+BEGIN_SRC sh
#!/bin/bash
dos2unix temp2.txt
sed -i "/[A-Za-z]/d" temp2.txt
#+END_SRC

**** Un plot molto piu' carino con data ora nelle ascisse
#+BEGIN_SRC sh
watch -n1 'printf "%s\n" "$(date +"%F_%T") $(cat /sys/class/thermal/thermal_zone0/temp)"| tee -a /tmp/temp_file.dat'
#+END_SRC
e
#+BEGIN_SRC sh
#!/usr/bin/gnuplot
reset

# wxt
set terminal wxt size 1200,800 enhanced font 'Verdana,9' persist
# png
#set terminal pngcairo size 410,250 enhanced font 'Verdana,9'
#set output 'nice_web_plot.png'
# svg
#set terminal svg size 410,250 fname 'Verdana, Helvetica, Arial, sans-serif' \
#fsize '9' rounded dashed
#set output 'nice_web_plot.svg'

# define axis
# remove border on top and right and set color to gray
set style line 11 lc rgb '#808080' lt 1
set border 3 back ls 11
set tics nomirror
# define grid
set style line 12 lc rgb '#808080' lt 0 lw 1
set grid back ls 12
# color definitions
set style line 1 lc rgb '#8b1a0e' pt 6 ps 1 lt 1 lw 2 # --- red
#set style line 2 lc rgb '#5e9c36' pt 6 ps 1 lt 1 lw 2 # --- green
set datafile separator ' '
set key top right

#Data format
set xdata time
set timefmt "%Y-%m-%d_%H:%M:%S"
set format x '%b %d %H:%M:%S'
set xtics rotate by 75 right

set xlabel 'Hours'
set ylabel 'Temperature'
set title "Temperature log"
plot '/tmp/temp_file.dat' u 1:2 t 'Cpu temperature' w lp ls 1,
#+END_SRC

**** Plot con refresh ogni secondo e 100 milli secondi di precisione

#+BEGIN_SRC sh
watch -n0.1 'printf "%s\n" "$(date +"%F_%T.%2N") $(cat /sys/class/thermal/thermal_zone0/temp)"| tee -a /tmp/temp_file.dat'
#+END_SRC

Gnuplot

#+BEGIN_SRC sh
#!/usr/bin/gnuplot
reset

# wxt
set terminal wxt size 1200,800 enhanced font 'Verdana,9' persist
# png
#set terminal pngcairo size 410,250 enhanced font 'Verdana,9'
#set output 'nice_web_plot.png'
# svg
#set terminal svg size 410,250 fname 'Verdana, Helvetica, Arial, sans-serif' \
#fsize '9' rounded dashed
#set output 'nice_web_plot.svg'

# define axis
# remove border on top and right and set color to gray
set style line 11 lc rgb '#808080' lt 1
set border 3 back ls 11
set tics nomirror
# define grid
set style line 12 lc rgb '#808080' lt 0 lw 1
set grid back ls 12
# color definitions
set style line 1 lc rgb '#8b1a0e' pt 6 ps 1 lt 1 lw 2 # --- red
#set style line 2 lc rgb '#5e9c36' pt 6 ps 1 lt 1 lw 2 # --- green
set datafile separator ' '
set key top right

#Data format
set xdata time
set timefmt "%Y-%m-%d_%H:%M:%S"
set format x '%b %d %H:%M:%.2S'
set xtics rotate by 75 right

set xlabel 'Hours'
set ylabel 'Temperature'
set title "Temperature log"
plot '/tmp/temp_file.dat' u 1:2 t 'Cpu temperature' w lp ls 1,
pause 1
reread
#+END_SRC

<2018-12-10 lun>
E' necessario andare a risolvere il problema in via software, la prima che ho fatto e' controllare come
il *pmic* della *sabresd* e' configurato rispetto al *pmic* di *gsd*. In questo caso i due device usano
lo stesso regolatore per la parte *ARM* della cpu definita sotto il nodo *cpu0* in un file *dtsi*. Questo
regolatore pero' e' configurato in maniera diversa per i due dispositivi, *gsd* ha la property
~regualtor-ramp-delay~ settata a 6250, mentre *sabresd* non ha questa property.

Per la parte *soc* della *cpu0* viene utilizzato un regolatore diverso, pero' questo regolatore e' configurato
in maniera ugale per entrambi i dispositivi.

Controllando lo schama delle due board, sembra che la topologia sia molto differente, per questo motivo
prima di iniziare a modificare il devicetree, cerco di trovare un modo per aumentare il rapporto frequenza
voltaggio dentro il kernel linux.
** DONE OverVolting CPU
   CLOSED: [2018-12-10 lun 16:51]
In teoria quello che sto cercando e' una struct contenente delle coppie freqeunza valore dentro la directory
~arch/arm/mach-imx~. Cercando in internet per la keyword =undervolt/overvolt= non ho trovato nessun
risultato significativo.

Guardando il codice sorgente e grepando per =voltage= in ~arch/arm/mach-imx~ ho trovato una config
*CONFIG_MX6_VPU_352M* con riferimento a [[https://community.nxp.com/thread/309304][thread]] dove aumentano il valore della freqeunza della *VPU* viene
aumentato anche il valore del voltaggio lasciato alla cpu.
#+BEGIN_SRC sh
[    0.000000] VPU 352M is enabled!
[    4.580866] remove 396MHz OPP for VPU running at 352MHz!
[    4.586934] increase SOC/PU voltage for VPU352MHz
#+END_SRC

Questi sono i messaggi stampati dal dmesgn quando la configurazione di prima viene attivata.
Quello che voglio provare a fare e' rimuovere l'overclocking della *VPU* lasciando abilitato
l'overvolt della cpu.

Quello che era sbalgliato era la configurazione nel device-tree della configurazione del pmic,
usando qualla corretta presa dalla *sabresd* il promblema si e' risolto.
** Scripts per il test
#+BEGIN_SRC sh
#!/bin/sh
#adb -s ea98b5781e0fc9d4 shell "echo userspace > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor"
#adb -s ea98b5781e0fc9d4 shell "echo $1 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_setspeed"
adb -s ea98b5781e0fc9d4 shell cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
adb -s ea98b5781e0fc9d4 shell cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq
adb -s ea98b5781e0fc9d4 shell "echo com.android.quicksearchbox > /data/misc/black-list.txt"
adb -s ea98b5781e0fc9d4 shell "echo com.android.soundrecorder >> /data/misc/black-list.txt"
adb -s ea98b5781e0fc9d4 shell "echo com.freescale.wfdsink >> /data/misc/black-list.txt"
adb -s ea98b5781e0fc9d4 shell "echo com.fsl.ethernet >> /data/misc/black-list.txt"
adb -s ea98b5781e0fc9d4 shell "echo com.freescale.cactusplayer >> /data/misc/black-list.txt"
adb -s ea98b5781e0fc9d4 shell monkey --pkg-blacklist-file /data/misc/black-list.txt \
--pct-syskeys 0 --pct-appswitch 90 -v 240000  --ignore-crashes \
--ignore-timeouts --ignore-security-exceptions --pct-nav 70 \
--pct-majornav 15 --pct-touch 5 --pct-trackball 5 --throttle 5 >  monkey.log
#+END_SRC

*Script per senza adb*
#+BEGIN_SRC sh
cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq
echo com.android.quicksearchbox > /data/misc/black-list.txt
echo com.android.soundrecorder >> /data/misc/black-list.txt
echo com.freescale.wfdsink >> /data/misc/black-list.txt
echo com.fsl.ethernet >> /data/misc/black-list.txt
echo com.freescale.cactusplayer >> /data/misc/black-list.txt
monkey --pkg-blacklist-file /data/misc/black-list.txt \
--pct-syskeys 0 --pct-appswitch 90 -v 240000  --ignore-crashes \
--ignore-timeouts --ignore-security-exceptions --pct-nav 70 \
--pct-majornav 15 --pct-touch 5 --pct-trackball 5 --throttle 5 >  /data/misc/monkey.log
#+END_SRC

** Note generali commandi
Per lanciare una applicazione da *adb* e' possibile usare
#+BEGIN_SRC sh
adb shell pm list package -f
#+END_SRC
Per printare tutta la lista delle app installate, e succesivamente
#+BEGIN_SRC sh
adb -s $1 shell monkey --pct-syskeys 0 -p $2 1
#+END_SRC
Per poter usare il tablet normalmente ho dovuto disabilitare tutte
le loro applicazioni tramite
#+BEGIN_SRC sh
pm disable <package>
#+END_SRC
