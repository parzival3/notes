/*
 * main.c
 *
 *  Created on: 10 April 2018
 *      Author: Emmanuel
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
//#include <termios.h>
#include <math.h>

#include <stdbool.h>

//#include <semaphore.h>
//#include <fcntl.h>
//#include <sys/stat.h>
//#include <pthread.h>

//#include <locale.h>

//#include <linux/fb.h>
//#include <sys/mman.h>
//#include <sys/ioctl.h>
#include <sys/time.h>
#include <string.h>
#include <errno.h>

#include <signal.h>
#include <getopt.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <linux/fb.h>
#include <time.h>
#include <limits.h>

#include <SDL2/SDL.h>
//#include <SDL2/SDL_events.h>
//#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_revision.h>

SDL_Window *WID_main_window;
SDL_Renderer *WID_main_renderer;

typedef struct
{
    unsigned long long int cumul;
    unsigned int count;
    struct timeval tv;
}chrono_t;

int getVideoInfos(void)
{
    int numVideoDrivers;
    int numRenderDrivers;

    int ret;

    int j;

    int index;

    SDL_RendererInfo info;

    printf("VIDEO DRIVER information:\n");

    numVideoDrivers = SDL_GetNumVideoDrivers();

    if(numVideoDrivers <= 0)
    {
        printf("Error occurs on SDL_GetNumVideoDrivers (%s)\n",SDL_GetError());
        return -1;
    }else {
        printf("Number of video drivers available: %d\n",numVideoDrivers);
    }

    for(index = 0; index < numVideoDrivers; index++) {
        printf("\tvideo_driver_index= %d\tname: %s\n",index,SDL_GetVideoDriver(index));
    }

    printf("\n");
    printf("RENDERER information:\n");

    numRenderDrivers = SDL_GetNumRenderDrivers();

    if(numRenderDrivers <= 0)
    {
        printf("Error occurs on SDL_GetNumRenderDrivers (%s)\n",SDL_GetError());
        return -1;
    }else {
        printf("Number of renderer available: %d\n",numRenderDrivers);
    }

    printf("details:\trenderer_index name (flags|texture_formats_number|texture_width|texture_height)\n");

    for(index = 0; index < numRenderDrivers; index++)
    {
        ret = SDL_GetRenderDriverInfo(index,&info);

        if(ret < 0)
        {
            printf("Error occurs on SDL_GetRenderDriverInfo (%s)\n",SDL_GetError());
            return -1;
        }else
        {
            printf("\n");
            printf("\trenderer_index= %d\tname: %s (%d|%d|%d|%d)\n",index,info.name,info.flags,info.num_texture_formats,info.max_texture_width,info.max_texture_height);
            printf("\tdetails:\n");

            for(j = 0; j < info.num_texture_formats; j++)
            {
                printf("\t\ttexture_format %d= %d\n",j+1,info.texture_formats[j]);
            }

        }
    }
    return 0;
}

void startChrono(chrono_t *chrono)
{
    gettimeofday(&(chrono->tv), NULL);
}

void stopChrono(chrono_t *chrono)
{
    struct timeval now,result;

    gettimeofday(&now, NULL);

    result.tv_sec = now.tv_sec - chrono->tv.tv_sec;
    result.tv_usec = now.tv_usec - chrono->tv.tv_usec;
    if(result.tv_usec < 0)
    {
        --result.tv_sec;
        result.tv_usec += 1000000;
    }

    chrono->cumul += result.tv_usec + 1000000 * result.tv_sec;
    chrono->count++;
}

void printChrono(const char* texte, chrono_t *chrono)
{
    double result;

    result = (double)chrono->cumul/(double)chrono->count/1000;

    printf("%s = %f msec",texte,result);
}

void initChrono(chrono_t *chrono)
{
    chrono->cumul = 0;
    chrono->count = 0;
}

SDL_Texture *WID_loadTexture( SDL_Renderer *renderer, char *name)
{
	char imgName[100];


	sprintf(imgName,"%s",name);

	SDL_Surface* surface = IMG_Load( imgName );

	if(surface == NULL){
	    printf("Error in function IMG_load %s\n", SDL_GetError());
	    return NULL;
	}

	SDL_Texture *texture = SDL_CreateTextureFromSurface( renderer, surface );

    if(texture == NULL){
	    printf("Error in function SDL_CreateTextureFromSurface %s\n", SDL_GetError());
	    return NULL;
	}


	SDL_FreeSurface( surface );

	return texture;
}

int testPerf(int loop_number,int flagStep, int fb_fd)
{
    int i;
    int dummy = 0;

    chrono_t back_copy1,back_copy2,back_present,alpha_copy1,alpha_copy2,alpha_present,over_copy,over_present;

    SDL_Texture* fond = WID_loadTexture(WID_main_renderer, "background.png");
    if(fond == NULL)
	    return 1;

    SDL_Texture* image_alpha = WID_loadTexture(WID_main_renderer, "image_alpha.png");
    if(image_alpha == NULL)
	    return 1;

    SDL_Texture* over = WID_loadTexture(WID_main_renderer, "over.png");
    if(fond == NULL)
	    return 1;

    SDL_Texture* tex_dwg = SDL_CreateTexture(WID_main_renderer,
					SDL_PIXELFORMAT_ARGB8888,
					SDL_TEXTUREACCESS_TARGET,
					800,
					480);

    if(tex_dwg == NULL){
	    printf("Error in function SDL_CreateTexture %s\n", SDL_GetError());
	    return 1;
	}

    SDL_SetTextureBlendMode(tex_dwg,SDL_BLENDMODE_NONE);

    while(1)
    {
        initChrono(&back_copy1);
        initChrono(&back_copy2);
        initChrono(&back_present);
        initChrono(&alpha_copy1);
        initChrono(&alpha_copy2);
        initChrono(&alpha_present);
        initChrono(&over_copy);
        initChrono(&over_present);

        for(i = 0; i < loop_number ; i++)
        {
            //background
            SDL_SetRenderTarget(WID_main_renderer,tex_dwg);

            startChrono(&back_copy1);
            SDL_RenderCopy( WID_main_renderer,fond, NULL, NULL );
            stopChrono(&back_copy1);

            SDL_SetRenderTarget(WID_main_renderer,NULL);

            startChrono(&back_copy2);
            SDL_RenderCopy( WID_main_renderer,tex_dwg, NULL, NULL );
            stopChrono(&back_copy2);

            startChrono(&back_present);
            
            dummy = 0; 
            if (ioctl(fb_fd, FBIO_WAITFORVSYNC, &dummy)) {
                perror("Error waiting for VSYNC");
                 return -1;
             }

            SDL_RenderPresent(WID_main_renderer);
            stopChrono(&back_present);

            if(flagStep)
            {
                printf("\rPress Enter to continue the test...");
                fflush(stdout);
                getc(stdin);
            }

            //alpha
            SDL_SetRenderTarget(WID_main_renderer,tex_dwg);

            startChrono(&alpha_copy1);
            SDL_RenderCopy( WID_main_renderer,image_alpha, NULL, NULL );
            stopChrono(&alpha_copy1);

            SDL_SetRenderTarget(WID_main_renderer,NULL);

            startChrono(&alpha_copy2);
            SDL_RenderCopy( WID_main_renderer,tex_dwg, NULL, NULL );
            stopChrono(&alpha_copy2);

            startChrono(&alpha_present);

            dummy = 0; 
            if (ioctl(fb_fd, FBIO_WAITFORVSYNC, &dummy)) {
                perror("Error waiting for VSYNC");
                 return -1;
             }


            SDL_RenderPresent(WID_main_renderer);
            stopChrono(&alpha_present);

            if(flagStep)
            {
                printf("\rPress Enter to continue the test...");
                fflush(stdout);
                getc(stdin);
            }

            
            //over
            SDL_SetRenderTarget(WID_main_renderer,NULL);

            startChrono(&over_copy);
            SDL_RenderCopy( WID_main_renderer,over, NULL, NULL );
            stopChrono(&over_copy);

            startChrono(&over_present);

            dummy = 0; 
            if (ioctl(fb_fd, FBIO_WAITFORVSYNC, &dummy)) {
                perror("Error waiting for VSYNC");
                return -1;
            }

           
            SDL_RenderPresent(WID_main_renderer);
            stopChrono(&over_present);

        if(flagStep)
        {
                printf("\rPress Enter to continue the test...%d%%",((i+1)*100)/loop_number);
                fflush(stdout);
                getc(stdin);
        }else
            {
                printf("\r############################## in progress: %d%%",((i+1)*100)/loop_number);
                fflush(stdout);
            }
        }
        printf("\n\nProcessing time for BACKGROUND\n");
        printChrono("RenderCopy_1",&back_copy1);
        printf("\t");
        printChrono("RenderCopy_2",&back_copy2);
        printf("\t");
        printChrono("RenderPresent",&back_present);

        printf("\nProcessing time for ALPHA\n");
        printChrono("RenderCopy_1",&alpha_copy1);
        printf("\t");
        printChrono("RenderCopy_2",&alpha_copy2);
        printf("\t");
        printChrono("RenderPresent",&alpha_present);

        printf("\nProcessing time for OVER\n");
        printChrono("RenderCopy",&over_copy);
        printf("\t");
        printChrono("RenderPresent",&over_present);

        printf("\n\n");
    }


    return 0;
}

int WID_init(int w, int h, int video_driver_index, int renderer_index)
{
  if(SDL_SetHint(SDL_HINT_RENDER_VSYNC, "1") != SDL_TRUE)
    {
        printf("Fail to execute SDL_SetHint %s\n", SDL_GetError());
        exit(1);
    }
    SDL_RendererInfo info;

    setenv("SDL_VIDEODRIVER",SDL_GetVideoDriver(video_driver_index),1);

	SDL_GetRenderDriverInfo(renderer_index,&info);
	if(SDL_SetHint(SDL_HINT_RENDER_DRIVER, info.name) != SDL_TRUE)
	{
	    printf("Fail to execute SDL_SetHint %s\n", SDL_GetError());
	    exit(1);
	}

//	printf("setting specific env variable for GL_driver\n");
//	setenv("SDL_VIDEO_GL_DRIVER", "usr/lib/arm-linux-gnueabihf/mesa-egl/libGLESv2.so.2",1);///
//    setenv("SDL_VIDEO_EGL_DRIVER", "/usr/lib/arm-linux-gnueabihf/mesa-egl/libEGL.so.1",1);
//    if (SDL_SetHint(SDL_HINT_FRAMEBUFFER_ACCELERATION, "openglesv2") != SDL_TRUE)
//    	printf("Fail to execute SDL_SetHint %s\n", SDL_GetError());

//    setenv("SDL_VIDEO_GL_DRIVER", "/usr/lib/libGLESv2.so",1);///usr/lib/arm-linux-gnueabihf/mesa-egl/libGLESv2.so.2
//    setenv("SDL_VIDEO_EGL_DRIVER", "/usr/lib/libGLESv2.so",1);

//	if (SDL_SetHint(SDL_HINT_FRAMEBUFFER_ACCELERATION, "0") != SDL_TRUE)
//    	printf("Fail to execute SDL_SetHint %s\n", SDL_GetError());


    if ( SDL_Init( SDL_INIT_VIDEO) < 0 )
        printf("Fail to execute SDL_Init %s\n", SDL_GetError());

    if ((WID_main_window = SDL_CreateWindow("iMoove4", 0, 0, 800, 480, SDL_WINDOW_SHOWN)) == NULL) {
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Couldn't create window %s", SDL_GetError());
        printf("%s\n", SDL_GetError());
        printf("Fail to execute SDL_CreateWindow %s\n", SDL_GetError());
        return 1;
    }

    SDL_GetRenderDriverInfo(renderer_index,&info);

    if ((WID_main_renderer = SDL_CreateRenderer(WID_main_window, renderer_index, info.flags)) == NULL) {
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Couldn't create renderer %s", SDL_GetError());
        printf("Fail to execute SDL_CreateRenderer %s\n", SDL_GetError());
        return 1;
    }

    if (WID_main_window == NULL)
    {
    	printf("Fail to execute WID_main_window %s\n", SDL_GetError());
    	exit(-1);
    }

    if (WID_main_renderer == NULL)
    {
    	printf("Fail to execute WID_main_renderer %s\n", SDL_GetError());
    	exit(-1);
    }

    //Curseur de souris
    //SDL_ShowCursor(SDL_ENABLE);
    //SDL_EventState(SDL_WINDOWEVENT,SDL_IGNORE);

	SDL_SetRenderDrawBlendMode(WID_main_renderer, SDL_BLENDMODE_BLEND);


    return(0);
}

static void sig(int sig)
{
	fflush(stderr);
	printf("signal %d caught\n", sig);
	fflush(stdout);
	exit(1);
}

static void info(void)
{
    SDL_version compiled;
    SDL_version running;

    SDL_VERSION(&compiled);

    SDL_GetVersion(&running);

    printf("SDL VERSION:\n");
    printf("Compiled version:\t%d.%d.%d\n",compiled.major, compiled.minor, compiled.patch);
    printf("Running version:\t%d.%d.%d\n",running.major, running.minor, running.patch);
    printf("\n");

    getVideoInfos();
}

static void help(void)
{
    printf("ALLCARE Innovations: testPerf_SDL2 v0.1 for evaluating SDL2 performance.\n");
	printf("\n");
	printf("Usage: testPerf [-i] [-v <video_driver_index>] [-r <renderer_index>] [-l <loop_number>]\n");
	printf("\n");
	printf("-i, info                Display information about SDL versions and video driver/renderer available on the platform\n");
	printf("\n");
	printf("-v, videodriver         Force using a specific video driver. Argument is the index number given by information command\n");
	printf("\n");
	printf("-r, renderer            Force using a specific video renderer. Argument is the index number given by information command\n");
	printf("\n");
	printf("-l, loop_number         Set the number of loop before printing the result for one pass.\n");
	printf("\n");
	printf("-s, step                Execute the test step by step waiting for user action between blit.\n");
	printf("\n");
	printf("-h, help                Print this help.\n");
	printf("\n");
}

int main(int argc, char **argv)
{

	SDL_RendererInfo render_info;

	int video_driver_index = 0;
	int renderer_index = 0;
	int loop_number = 100;
	int flagStep = 0;
    int fb_fd = 0;

    signal(SIGSEGV, sig);
	signal(SIGINT, sig);
	signal(SIGTERM, sig);

	while (1) {
		const struct option long_options[] = {
			{ "help",           no_argument,        NULL, 'h' },
			{ "info",           no_argument,        NULL, 'i' },
			{ "step",           no_argument,        NULL, 's' },
			{ "videodriver",    required_argument,  NULL, 'v' },
			{ "renderer",       required_argument,  NULL, 'r' },
			{ "loop_number",    required_argument,  NULL, 'l' },
		};

		int option_index = 0;
		int c = getopt_long(argc, argv, "hisv:r:l:", long_options, &option_index);

		errno = 0;
		if(c == -1)
		break;

		switch (c) {
            case 'h':
                help();
                return 0;
            case 'i':
                info();
                return 0;
            case 's':
                flagStep = 1;
                break;
            case 'v':
                if(optarg)
                {
                    video_driver_index = atoi(optarg);
                    if( (video_driver_index < 0) || (video_driver_index > 10) || (video_driver_index >= SDL_GetNumVideoDrivers() ) )
                    {
                       printf("video_driver_index out of range. Using default instead\n");
                       video_driver_index = 0;
                    }
                }else
                {
                    printf("video_driver_index not given. Using default instead\n");
                    video_driver_index = 0;
                }
                break;
            case 'r':
                if(optarg)
                {
                    renderer_index = atoi(optarg);
                    if( (renderer_index < 0) || (renderer_index > 10) || (renderer_index >= SDL_GetNumRenderDrivers() ) )
                    {
                       printf("renderer_index out of range. Using default instead\n");
                       renderer_index = 0;
                    }
                }else
                {
                    printf("renderer_index not given. Using default instead\n");
                    renderer_index = 0;
                }
                break;
            case 'l':
                if(optarg)
                {
                    loop_number = atoi(optarg);
                    if( (loop_number < 0) || (loop_number > 1000) )
                    {
                       printf("loop_number out of range. Using default instead\n");
                       loop_number = 100;
                    }
                }else
                {
                    printf("loop_number not given. Using default instead\n");
                    loop_number = 100;
                }
                break;
            default:
                help();
                return 0;
		}

		if (errno) {
			char *str = "option ?";
			str[7] = c & 0xff;
			perror(str);
		}
	}
    
    fb_fd = open("/dev/fb0" , O_RDWR);
    if (fb_fd < 0) {
        perror("Cannot Open framebuffer device:");
        exit(1);
    } 
    
	WID_init(800, 480,video_driver_index,renderer_index);

	SDL_GetRendererInfo(WID_main_renderer,&render_info);

    printf("Launching testPerf using:\n");
	printf("\tVIDEO DRIVER: name:%s\n",SDL_GetVideoDriver(video_driver_index));
	printf("\tRENDERER: name:%s\n\t\tflags: software:%s accelerated:%s preventvsync:%s targettexture:%s\n",render_info.name,
                        (render_info.flags&SDL_RENDERER_SOFTWARE)?"YES":"NO",
                        (render_info.flags&SDL_RENDERER_ACCELERATED)?"YES":"NO",
                        (render_info.flags&SDL_RENDERER_PRESENTVSYNC)?"YES":"NO",
                        (render_info.flags&SDL_RENDERER_TARGETTEXTURE)?"YES":"NO");

	printf("\n");


	testPerf(loop_number,flagStep, fb_fd);

    return 0;

}

/*
SDL_Texture *WID_loadTexture2( SDL_Renderer *renderer, char *name)
{
	char imgName[100];


	sprintf(imgName,"%s",name);

	SDL_Surface* surface = IMG_Load( imgName );

	SDL_Texture *texture = SDL_CreateTextureFromSurface( renderer, surface );
//
//	SDL_Texture *texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGB888, SDL_TEXTUREACCESS_TARGET, 800, 600);
//    SDL_UpdateTexture(texture, NULL, surface->pixels, 800 * sizeof (Uint32));



	SDL_FreeSurface( surface );

	return texture;
}

int testPerf_old(void)
{
    unsigned long long int cumul_temps_1,cumul_temps_2;
    unsigned long long int count_temps_1,count_temps_2;
    double result_1,result_2;
    int debug_temps;
    struct timeval tv1,tv2,tv3;
    int i;

    uint32_t format;
    int access,wi,he;
    uint8_t alpha,r,g,b;
    SDL_BlendMode bm;

    SDL_Texture* tex1 = WID_loadTexture2(WID_main_renderer, "fond_800_600.png");


    SDL_QueryTexture(tex1, &format, &access, &wi, &he);
    printf("format=%d access=%d w=%d h=%d\n",format,access,wi,he);
    printf("%d\t",SDL_PIXELTYPE(format));
    printf("%d\t",SDL_PIXELORDER(format));
    printf("%d\t",SDL_PIXELLAYOUT(format));
    printf("%d\t",SDL_BITSPERPIXEL(format));
    printf("%d\t",SDL_BYTESPERPIXEL(format));
    printf("%d\t",SDL_ISPIXELFORMAT_INDEXED(format));
    printf("%d\t",SDL_ISPIXELFORMAT_ALPHA(format));
    printf("%d\n",SDL_ISPIXELFORMAT_FOURCC(format));

    SDL_GetTextureAlphaMod(tex1,&alpha);
    printf("alpha = %d\n",alpha);

    SDL_GetTextureBlendMode(tex1,&bm);
    printf("blend mode = %d\n",bm);

    SDL_GetTextureColorMod(tex1,&r,&g,&b);
    printf("r = %d g = %d b = %d\n",r,g,b);

    SDL_Texture* tex2 = WID_loadTexture2(WID_main_renderer, "fond_800_600_transparant.png");

    SDL_Texture* texgrille = WID_loadTexture2(WID_main_renderer, "grille_800x600_alpha.png");

    SDL_Texture* tex_fond = WID_loadTexture2(WID_main_renderer, "fond_800_600.png");

    SDL_QueryTexture(tex2, &format, &access, &wi, &he);
    printf("format=%d access=%d w=%d h=%d\n",format,access,wi,he);
    printf("%d\t",SDL_PIXELTYPE(format));
    printf("%d\t",SDL_PIXELORDER(format));
    printf("%d\t",SDL_PIXELLAYOUT(format));
    printf("%d\t",SDL_BITSPERPIXEL(format));
    printf("%d\t",SDL_BYTESPERPIXEL(format));
    printf("%d\t",SDL_ISPIXELFORMAT_INDEXED(format));
    printf("%d\t",SDL_ISPIXELFORMAT_ALPHA(format));
    printf("%d\n",SDL_ISPIXELFORMAT_FOURCC(format));

    SDL_GetTextureAlphaMod(tex2,&alpha);
    printf("alpha = %d\n",alpha);

    SDL_GetTextureBlendMode(tex2,&bm);
    printf("blend mode = %d\n",bm);

    SDL_GetTextureColorMod(tex2,&r,&g,&b);
    printf("r = %d g = %d b = %d\n",r,g,b);

    SDL_Texture* tex_dwg1 = SDL_CreateTexture(WID_main_renderer,
					SDL_PIXELFORMAT_ARGB8888,
					SDL_TEXTUREACCESS_TARGET,
					800,
					600);

//    SDL_SetTextureBlendMode(tex_dwg1,SDL_BLENDMODE_BLEND);

    SDL_Texture* tex_dwg2 = SDL_CreateTexture(WID_main_renderer,
					SDL_PIXELFORMAT_ARGB8888,
					SDL_TEXTUREACCESS_TARGET,
					800,
					600);

//    SDL_SetTextureBlendMode(tex_dwg2,SDL_BLENDMODE_BLEND);

    SDL_SetTextureBlendMode(tex1,SDL_BLENDMODE_NONE);
    SDL_SetTextureBlendMode(tex2,SDL_BLENDMODE_NONE);
    SDL_SetTextureBlendMode(tex_dwg1,SDL_BLENDMODE_NONE);
    SDL_SetTextureBlendMode(tex_dwg2,SDL_BLENDMODE_NONE);


//    print_debug("renderer1init",NULL,WID_main_renderer);
//    print_debug("tex1init",tex1,NULL);
//    print_debug("tex2inti",tex2,NULL);

//    return 0;

//    SDL_SetRenderDrawBlendMode(WID_main_renderer, SDL_BLENDMODE_NONE);

    cumul_temps_1 = 0;
    count_temps_1 = 0;
    result_1 = 0;
    cumul_temps_2 = 0;
    count_temps_2 = 0;
    result_2 = 0;

    SDL_Rect src={0,0,400,300};
    SDL_Rect dst={0,0,400,300};
    SDL_Rect rect={0,0,800,600};

    while(1)
    {
        for(i=0;i<500;i+=1)
        {

            if(i%2)
            {
                gettimeofday(&tv1, NULL);

                SDL_SetRenderTarget(WID_main_renderer,tex_dwg1);
//                SDL_SetRenderDrawBlendMode(WID_main_renderer, SDL_BLENDMODE_BLEND);

                SDL_RenderCopy( WID_main_renderer,tex1, NULL, NULL );

//                gettimeofday(&tv2, NULL);
//                printf("temps 1 = %fmsec\n",(float)(((float)(tv2.tv_usec - tv1.tv_usec))/1000));
//                gettimeofday(&tv1, NULL);

                SDL_SetRenderTarget(WID_main_renderer,tex_dwg2);
                SDL_SetRenderDrawBlendMode(WID_main_renderer, SDL_BLENDMODE_NONE);

//
//                SDL_SetRenderDrawColor(WID_main_renderer,255,0,0,128);
////                SDL_RenderFillRect(WID_main_renderer,&rect);
//                SDL_RenderClear(WID_main_renderer);


                SDL_RenderCopy( WID_main_renderer,tex2, NULL, NULL );


//                gettimeofday(&tv2, NULL);
//                printf("temps 2 = %fmsec\n",(float)(((float)(tv2.tv_usec - tv1.tv_usec))/1000));
//                gettimeofday(&tv1, NULL);

                SDL_SetRenderTarget(WID_main_renderer,NULL);
//                SDL_SetRenderDrawBlendMode(WID_main_renderer, SDL_BLENDMODE_BLEND);



//                SDL_SetTextureBlendMode(tex_dwg1,SDL_BLENDMODE_BLEND);

                SDL_RenderCopy( WID_main_renderer,tex_dwg1, NULL, NULL );

//                SDL_SetRenderDrawBlendMode(WID_main_renderer, SDL_BLENDMODE_BLEND);

//                gettimeofday(&tv2, NULL);
//                printf("temps 3 = %fmsec\n",(float)(((float)(tv2.tv_usec - tv1.tv_usec))/1000));
//                gettimeofday(&tv1, NULL);

                SDL_SetTextureBlendMode(tex_dwg2,SDL_BLENDMODE_BLEND);
                SDL_RenderCopy( WID_main_renderer,tex_dwg2, NULL, NULL );

//                SDL_SetRenderDrawBlendMode(WID_main_renderer, SDL_BLENDMODE_BLEND);

//                gettimeofday(&tv2, NULL);
//                printf("temps 4 = %fmsec\n",(float)(((float)(tv2.tv_usec - tv1.tv_usec))/1000));
//                gettimeofday(&tv1, NULL);

                SDL_RenderPresent(WID_main_renderer);

                gettimeofday(&tv2, NULL);
//                printf("temps 5 = %fmsec\n",(float)(((float)(tv2.tv_usec - tv1.tv_usec))/1000));


                if(tv2.tv_usec > tv1.tv_usec){
                    cumul_temps_1 += tv2.tv_usec - tv1.tv_usec;
                    count_temps_1++;
                }

//                print_debug("tex1",tex1,NULL);
//                print_debug("renderertex1",NULL,WID_main_renderer);
            }
            else
            {
                gettimeofday(&tv1, NULL);
//                SDL_SetRenderTarget(WID_main_renderer,tex3);
//                SDL_RenderCopy( WID_main_renderer,tex2, NULL, NULL );

                SDL_SetRenderTarget(WID_main_renderer,NULL);
                SDL_RenderCopy( WID_main_renderer,tex_fond, NULL, NULL );

//                gettimeofday(&tv2, NULL);

                SDL_RenderPresent(WID_main_renderer);

//                printf("temps SDL_RenderCopy_2 = %fmsec\n",(float)(((float)(tv2.tv_usec - tv1.tv_usec))/1000));


//                SDL_SetRenderTarget(WID_main_renderer,NULL);
//                SDL_RenderCopy( WID_main_renderer,tex2, NULL, NULL );
                gettimeofday(&tv2, NULL);
                if(tv2.tv_usec > tv1.tv_usec){
                    cumul_temps_2 += tv2.tv_usec - tv1.tv_usec;
                    count_temps_2++;
                }
//                printf("temps SDL_RenderCopy_2 = %fmsec\n",(float)(((float)(tv2.tv_usec - tv1.tv_usec))/1000));
//                print_debug("tex2",tex2,NULL);
//                print_debug("renderertex2",NULL,WID_main_renderer);
            }


//            SDL_RenderPresent(WID_main_renderer);


//            SDL_Delay(1000);

//            gettimeofday(&tv3, NULL);


//            if(tv3.tv_usec > tv2.tv_usec){
//                cumul_temps_p += tv3.tv_usec - tv2.tv_usec;
//                count_temps_p++;
//            }

//            printf("temps SDL_RenderCopy = %fmsec \tSDL_RenderPresent = %f msec\n",(float)(((float)(tv2.tv_usec - tv1.tv_usec))/1000),(float)(((float)(tv3.tv_usec - tv2.tv_usec))/1000));
        }

        result_1 = (double)cumul_temps_1/(double)count_temps_1/1000;
        result_2 = (double)cumul_temps_2/(double)count_temps_2/1000;

        printf("temps Process_1 = %fmsec \tProcess_2 = %f msec\n",result_1,result_2);


//        result_c = (double)cumul_temps_c/(double)count_temps_c/1000;
//        printf("temps SDL_RenderCopy + SDL_RenderPresent = %f msec\n",result);

        SDL_Delay(1000);
    }

    return 0;
}
*/




