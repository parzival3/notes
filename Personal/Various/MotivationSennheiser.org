#+TITLE: Motivation letter
To whom it may concern,

When I saw the job opportunity in the DTU University page, I was captured by the possibility to combine two things that fascinate me, automated testing and embedded systems.
In my previous job in Italy, I had the opportunity to get involved with both of them, and I believe this job will give me the chance to expand and improve my knowledge in these fields.
\\
Before starting my master's at DTU, I worked for one year as a software developer in Dave Embedded System, a company that produces SBC (Single Board Computers) and SOM (System On Module). During this time,  I learned how necessary tests are for the code and embedded devices and how things can go wrong if the tests are inadequate. Testing is hard and creating functional tests even harder, but that's why I find it fascinating. Since the company I have worked for was small, the number of devices produced was low, and most of the tests were carried out by operators. That left me curious as to how large embedded systems manufacturers manage to scale and automate their test environment to provide industry standard quality control for their products.  Learning how to develop and maintain a test environment in a scalable way is the main reason why I'm applying for this job. Given the small size of Dave, I had the opportunity to experiment with different things. I learned how to use the test suite Avocado for automating part of the test bench for the SBCs produced, how to create and modify Jenkins pipelines for automating the build process of Linux Kernels, U-boot, and Yocoto file systems.  Moreover, I learned how to use the CI/CD functionality of Gitlab, and since then all my personal and University projects are stored in Gitlab. When it's possible, I enjoy creating tests and pipelines to improve upon the consistency and quality of my own and my groupmates' code.

\\
I appreciate your time and consideration and hope that my skills and experience align with your needs.

Kind Regards,
Enrico
* Structure                                                        :noexport:
- Introduction
- Testing is Hard
  - Working experience with DAVE
  - Hard but it is the only motivation to automate
  - Article on Bunny and testing hardware
- Competence
  - I don't know all the requirements
  - But I'm willing to learn
  - Maintenance

Before starting working in my previous company, I never realized how important
testing is. Since the development of my first driver for an MFD device I learned
how many things can go wrong in the development of a product if it is not
properly tested. Since it was a small embedded company I had the opportunity to
be involved in all the various steps of the development of a project. There I
learn how to use Jenkins to build Kernels, U-boot and Linux RFS with Bitbake.
There I learn how to create pipeline in Gitalb and now every project that I do
for my university is stored in Gitlab and implements a pipeline in order to
check the code of all the group mate that I'm working with.
In my day to day life I always try to reduce the amount of repetitive work by
using python and shell scriting. 
It was a small embedded company we didn'tTesting is hard especially for embedded systems, but that is the
main reason because I find it interesting. On my previous work I had experience
with the Avocado test suite and and Jenkins, since I discovered the powerful
Gitlab suite, all my university project are moved and implement a pipeline.
Since I didn't work on large project testing was just a
something you had to do to show to the teacher. But when I started working on my
previous company everything changed. There I understood what a pipeline is, how
to use CI/CD tools and I never went back. Right now all the new project that I
have to do with my university are hosted on Gitlab so I can use the CI/CD tools
in order to test every commit that I push to the repository.
But then I suddenly realize how important it is, and my
previous company was a small company, I always think about big companies that
have to test and automate millions of product.

Hardware testing is hard but that's why it is interesting to me.

Bunny blog is (chrome-extension://klbibkeccnjlkjkiokjodocebajanakg/suspended.html#ttl=Exclave%3A%20Hardware%20Testing%20in%20Mass%20Production%2C%20Made%20Easier%20%C2%AB%20bunnie's%20blog&pos=15685&uri=https://www.bunniestudios.com/blog/?p=5450)[here]

