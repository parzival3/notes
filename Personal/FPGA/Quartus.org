#+TITLE: Quartus note

* Creating a project from zero

The file ~*.qsf~ and ~.qpf~ are the file that contain the project description.
Make sure that the ~qpf~ file reference to the correct ~qsf~ file in the
*PROJECT_REVISION* variables.
