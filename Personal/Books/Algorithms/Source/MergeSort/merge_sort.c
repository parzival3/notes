#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define LEN 10

void copy_array(int *A, int *B, int len);
void merge(int *A, int left, int right, int medium);
void merge_sort(int *A, int left, int right);
void new_merge(int *A, int len, int mid);
void new_merge_sort(int *A, int len);
void print_array(int *A, int len, char name);

int main(int argc, char **argv)
{
        int A[LEN] = {1, 5, 3, 7, 0, 8, -1, 12, 44, 29};
        print_array(A, LEN, 'A');
        new_merge_sort(A, LEN);
        printf("---------------------------------\n");
        print_array(A, LEN, 'A');
        return EXIT_SUCCESS;
}

void separation()
{
        printf("--------------------------------\n");
}

void merge_sort(int *A, int left, int right)
{
        int medium = (left + right) / 2;
        if (left < right) {
                merge_sort(A, left, medium);
                merge_sort(A, medium + 1, right);
                merge(A, left, right, medium);
        }
}

void new_merge_sort(int *A, int len)
{
        int medium = len / 2;
        if (len > 1)
        {
                new_merge_sort(A, medium);
                new_merge_sort(A + medium, len - medium);
                new_merge(A, len, medium);
        }
}

void merge(int *A, int left, int right, int medium)
{
        int i = left; int j = medium + 1; int k = 0;
        int B[50];

        while(i <= medium && j <= right)
        {
                if(A[i] < A[j])
                        B[k++] = A[i++];
                else
                        B[k++] = A[j++];
        }

        while(i <= medium)
                B[k++] = A[i++];

        while(j <= right)
                B[k++] = A[j++];

        for(i = left, j = 0; i <= right; i++, j++)
                A[i] = B[j];

        separation();
        print_array(B, right - left + 1, 'B');
}

void new_merge(int *A, int len, int mid)
{
        int i = 0, j = mid + 1, k;
        int *B = calloc(len, sizeof(int));

        for (k = 0; k < len; k++) {
                if (j > len)
                        B[k] = A[i++];
                else if (i > mid)
                        B[k] = A[j++];
                else if (A[i] < A[j])
                        B[k] = A[i++];
                else
                        B[k] = A[j++];
        }

        for (k = 0; k < len; k++)
                A[k] = B[k];
}

void print_array(int *A, int len, char name)
{
        int i;
        for (i = 0; i < len; i++)
                printf("%c[%d] = %d\n", name, i, A[i]);
}
